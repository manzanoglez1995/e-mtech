
<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>



## E-mtech
# Pasos de instalación

- git clone
- composer install
- copy .env.example .env
- llenar .env
- php artisan migrate:install
- php artisan migrate
- php artisan migrate --seed
- php artisan passport:instal
- php artisan synchronize:product 


* para desarrollo:
- npm install (solo local)
- npm run watch (solo local)





