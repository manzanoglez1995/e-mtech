<?php

namespace App\Console\Commands;

use App\Http\Controllers\Api\ProductController;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class synchronize extends Command
{

    protected $signature = 'synchronize:product';

    protected $description = 'Comando para actualizar la BD con el XML de CVA';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
    
        try{
        $this->info("Descargando XML..");
        $url = Product::URL . "cliente=" . Product::CLIENT . "&MonedaPesos=1&exist=3";


          //  $xml = simplexml_load_file($url);

        // load as string
        $xml = file_get_contents($url);

        $string = trim(preg_replace('/\s+/', ' ', $xml));

        if($string===""){

            return $this->info("Sin resultados encontrados");
        }

     //   $xml = simplexml_load_string($xmlstr);

            $countproducts = 0;

            $this->info("Actualizando base de datos");

            foreach ($xml as $x) {

                $product = Product::where('key', $x->clave)->first();

                if (!$product) {
                    $product = new Product();
                }

                $product->key = $x->clave;
                $product->manufacturer_code = $x->codigo_fabricante;
                $product->description = $x->descripcion;
                $product->principal = $x->principal;
                $product->group = $x->grupo;
                $product->brand = $x->marca;
                $product->warranty = $x->garantia;
                $product->class = $x->clase;
                $product->available = $x->disponible;
                $product->price = $x->precio;
                $product->currency = $x->moneda;
                $product->data_sheet = $x->ficha_tecnica;
                $product->bussiness_card = $x->ficha_comercial;
                $product->image = $x->imagen;
                $product->AvailableCD = $x->disponibleCD;

                $product->save();
                $countproducts++;

                $this->info("synchronized: $countproducts");
            }

            $totalXml = count($xml);
            $this->info("Exito");
            $this->info("Total synchronized: $totalXml");

        }catch(\Exception $e){
            $this->info($xml);
        }
    }
}
