<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const CLIENT = 1;
    const ADMIN = 2;

    protected $fillable = [
        'name', 'email', 'password','cellphone'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function billing()
    {
        return $this->hasMany(UserBilling::class);
    }

    public function shippingAdress()
    {
        return $this->hasMany(UserShippingAddresses::class);
    }

    public function creditCards()
    {
        return $this->hasMany(CreditCard::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

}
