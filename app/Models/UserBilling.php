<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBilling extends Model
{
    protected $hidden = [
        'user_id','active'
    ];


    protected $fillable = [
        'RFC', 'nameOrSocialReason', 'user_id','active'
    ];

}
