<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const WSDL = 'https://www.grupocva.com/pedidos_web/pedidos_ws_cva.php?wsdl';

    const status = [
        'pending',//0
        'approved',//1
        'cancel',//2
        'rejected',//3
        'refund',//4
        'fail',//5
        'purchased_cva',//6
        'order_sent', //7
        'Finalize', //8
    ];


    protected $fillable = [
        'user_id',
        'item_qty',
        'tracking_number',
        'company',
        'total',
        'subtotal',
        'vat',
        'adress_id',
        'creditCard_id',
        'billing_id',
        'status',
        'item_qty'
    ];


    public function adressUser()
    {
        return $this->hasOne(UserShippingAddresses::class, 'id', 'adress_id');
    }

    public function billingUser()
    {
        return $this->hasOne(UserBilling::class, 'id', 'billing_id');
    }

    public function creditCardUser()
    {
        return $this->hasOne(CreditCard::class, 'id', 'creditCard_id');
    }

    static function getStatusOrderSpanish($status, $rol)
    {


        switch ($status) {
            case 'pending':
                if ($rol == User::CLIENT) {
                    $status = 'Pendiente';
                } else {
                    $status = 'Pendiente';
                }
                break;
            case 'approved':
                if ($rol == User::CLIENT) {
                    $status = 'Aprobado';
                } else {
                    $status = 'Aprobado';
                }
                break;
            case 'cancel':
                if ($rol == User::CLIENT) {
                    $status = 'Cancelado';
                } else {
                    $status = 'Cancelado';
                }
                break;
            case 'rejected':
                if ($rol == User::CLIENT) {
                    $status = 'rechazado';
                } else {
                    $status = 'rechazado';
                }
                break;
            case 'refund':
                if ($rol == User::CLIENT) {
                    $status = 'reembolso';
                } else {
                    $status = 'reembolso';
                }
                break;
            case 'fail':
                if ($rol == User::CLIENT) {
                    $status = 'Fallido';
                } else {
                    $status = 'Fallido';
                }
                break;
            case 'purchased_cva':
                if ($rol == User::CLIENT) {
                    $status = 'En espera de envio';
                } else {
                    $status = 'Comprado a CVA';
                }
                break;
            case 'order_sent':
                if ($rol == User::CLIENT) {
                    $status = 'Pedido enviado';
                } else {
                    $status = 'Pedido enviado';
                }
                break;
            case 'Finalize':
                if ($rol == User::CLIENT) {
                    $status = 'Pedido finalizado';
                } else {
                    $status = 'Pedido finalizado';
                }
                break;

        }

        return $status;
    }

    static function createOrderCVA($order){


        while (true) {

            if ($order) {

                if ($order->status == 'approved') {

                    $xmlOrder = "<PEDIDO>";
                    $xmlOrder .= "<NumOC>Orden $order->id</NumOC>";
                    $xmlOrder .= "<Paqueteria>0</Paqueteria>";
                    $xmlOrder .= "<CodigoSucursal>1</CodigoSucursal>";
                    $xmlOrder .= "<PedidoBO>N</PedidoBO>";
                    $xmlOrder .= "<Observaciones>n/a</Observaciones>";
                    $xmlOrder .= "<productos>";


                    foreach (json_decode($order->item_qty) as $product) {

                        $xmlOrder .= "<producto>";
                        $xmlOrder .= "<clave>$product->item</clave>";
                        $xmlOrder .= "<cantidad>$product->quantity</cantidad>";
                        $xmlOrder .= "</producto>";

                    }

                    $xmlOrder .= "  </productos>";
                    $xmlOrder .= "<TipoFlete>SF</TipoFlete>";

                    /*     $xmlOrder .= "<Calle>$street</Calle>";
                           $xmlOrder .= "<Numero>$external_number</Numero>";
                           $xmlOrder .= "<NumeroInt>$internal_number</NumeroInt>";
                           $xmlOrder .= "<Colonia>$colony</Colonia>";
                           $xmlOrder .= "<CP>$cp</CP>";
                           $xmlOrder .= "<Estado>1</Estado>";
                           $xmlOrder .= "<Ciudad>1</Ciudad>";
                           $xmlOrder .= "<Atencion>Atencion</Atencion>";
                    */

                    $xmlOrder .= "<TipoEnvio>1</TipoEnvio>";
                    $xmlOrder .= "</PEDIDO > ";

                    $cva = new \SoapClient(Order::WSDL);

                    $cva_order = $cva->PedidoWeb(Product::USER, Product::PWD, $xmlOrder);

                    //   dd($cva_order);

                    //agregar folio de cva a orden

                    if (isset($cva_order['pedido'])) {

                        $order->order_cva = $cva_order['pedido'];
                        $order->status = Order::status[6];
                        $order->save();

                        return true;

                    }




                } else {

                    //Pago no aprovado
                    return false;

                }

            }

        }

//orden no encontrada
        return false;

    }



    public function scopeStatus($query, $status)
    {
        $query->where('status', $status);
    }


}
