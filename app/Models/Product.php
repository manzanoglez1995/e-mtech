<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use MarkWilson\XmlToJson\XmlToJsonConverter;


class Product extends Model
{
    const URL = 'http://www.grupocva.com/catalogo_clientes_xml/lista_precios.xml?';
    const CLIENT = '26982';
    const USER = 'Admin26982';
    const PWD = 'Leonlarregui87';


    /*
    'clave'                =   'key'
    'codigo',   =   'manufacturer_code
    'descripcion'          =   'description'
    'principal'            =   'principal'
    'grupo'                =   'group'
    'marca'                =   'brand'
    'garantia'             =   'warranty'
    'clase'                =   'class'
    'disponible'           =   'available'
    'precio'               =   'price'
    'moneda'               =   'currency'
    'ficha_tecnica'        =   'data_sheet'
    'ficha_comercial'      =   'bussiness_card'
    'imagen'               =   'image'
    'disponibleCD'         =   'AvailableCD'
     */


    protected $fillable = [
        'key',
        'manufacturer_code',
        'description',
        'principal',
        'group',
        'brand',
        'warranty',
        'class',
        'available',
        'price',
        'currency',
        'data_sheet',
        'bussiness_card',
        'image',
        'AvailableCD',
    ];


    static function hasAvailableMin($min)
    {
        return static::where('available', '>=', $min);
    }

    function readXmlProduct(Request $request)
    {


        $cliente = $request->cliente;
        $marca = $request->marca;
        $grupo = $request->grupo;
        $clave = $request->clave;
        $codigo = $request->codigo;


        $url = Product::URL . "cliente=" . $cliente .
            "&marca=" . $marca .
            "&grupo=" . $grupo .
            "&clave=" . $clave .
            "&MonedaPesos=1" .
            "&codigo=" . $codigo;


        $xml = simplexml_load_file($url);
    }


    static function findXMl($key, $tc = 1)
    {

        $client = Product::CLIENT;
        $converter = new XmlToJsonConverter();

        $url = Product::URL .
            "cliente=" . $client .
            "&clave=" . $key .
            "&MonedaPesos=1" .
            "&tc=" . $tc;


        $xml = new \SimpleXMLElement(file_get_contents($url));

        $json = json_decode($converter->convert($xml));

        if (isset($json->articulos->item)) {

            return $json->articulos->item;
        }


        return false;

    }

    static function scopeHasAvailableMin($query,$min)
    {
      $query->where('available', '>=', $min);
    }



    public function scopeGroup($query, $group, $operation = "=")
    {

        if($operation == "like"){

            $query->Where('group', $operation , "%$group%" );

        }else{

            $query->Where('group', $group );
        }

    }

    public function scopeBrand($query, $brand,$operation = "=")
    {
        if($operation == "like"){

            $query->Where('brand', $operation , "%$brand%" );

        }else{

            $query->Where('brand', $brand);
        }
    }

    public function scopeKey($query, $key)
    {
        $query->OrWhere('key', 'like', "%$key%");
    }

    public function scopeDescription($query, $description,$operation = "=")
    {
        if($operation == "like"){

            $query->OrWhere('brand', $operation , "%$description%" );

        }else {

            $query->Where('description', 'like', "%$description%");
        }
    }

    public function scopeManufacturerCode($query, $manufacturerCode)
    {
        $query->OrWhere('manufacturer_code','like', "%$manufacturerCode%");
    }




    public function scopePrice($query, $min, $max)
    {

        if ($max == 10000) {
            $max = 9999999;
        }

        $query->whereBetween('price', array(is_null($min) ? 0 : $min, is_null($max) ? 9999999 : $max));
    }

    static function priceWithExtraCharge($product, $iva = 0.16){

        $extraCharge = ExtraCharge::where('group', $product->group)->first();


        if ($extraCharge) {

            // suma el porcentaje de e-mtech
            $product->price = $product->price + $product->price * ($extraCharge->commission_price / 100);

        }

        $product['iva'] = $product->price * $iva;

        $product['shipping_price'] = $extraCharge->shipping_price;

        //suma iva
        $product->price = floatval(sprintf(" % .2f", $product->price + $product['iva']));


        return $product;

    }

    static function convertDollarToMXN($price, $dls = 19.2607)
    {
        return floatval(sprintf(" % .2f", ($price * $dls)));
    }

    static function totalPrice($price)
    {
        return floatval(sprintf(" % .2f", $price));
    }

    static function formatPrice($price)
    {
        return floatval(sprintf(" % .2f", $price));
    }


    static function calculateIVA($price, $iva = 0.16)
    {
        return floatval(sprintf(" % .2f", ($price * $iva)));
    }

    static function calculatePriceWithoutIVA($price, $iva = 0.16)
    {
        return floatval(sprintf(" % .2f", $price - ($price * $iva)));
    }

    static function calcularteSubtotal($total,$iva)
    {
        return floatval(sprintf(" % .2f", $total - $iva));
    }



    //metodos de busqueda




}


