<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{

    protected $hidden =[
        'name','expireMonth','expireYear','user_id'
    ];

    protected $fillable = [
      'number','name','expireMonth','expireYear','type','user_id'
    ];
}
