<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserShippingAddresses extends Model
{
    public function state()
    {
        return $this->belongsTo(Estates::class,'estate_id','id');
    }
}
