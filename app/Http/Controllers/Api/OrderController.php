<?php

namespace App\Http\Controllers\Api;

use App\Mail\changeStatusOrder;
use App\Mail\NotifyCustomerPurchase;
use App\Mail\NotifyPurchaseAdministrators;
use App\Models\Estates;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use PayPal\Api\ExecutePayment;


class OrderController extends Controller
{


    public function show()
    {
        $orders = Order::orderBy('id', 'DESC')->where('status', '!=', Order::status[0])->paginate(10);

        if ($orders) {

            foreach ($orders as $order) {

                $order->item_qty = json_decode($order->item_qty);
                $order->payment_method = json_decode($order->payment_method);
                $order->user = json_decode($order->user);
                $order->shipping = json_decode($order->shipping);
                $order->direction = json_decode($order->direction);
                $order->billing = json_decode($order->billing);


                $order->direction->state = Estates::find($order->direction->estate_id);

                $order->status = Order::getStatusOrderSpanish($order->status, User::CLIENT);


            }


            return Response::json(array('success' => $orders), 200);
        }
        return Response::json(array('success' => 'Sin resultados'), 200);
    }


    public function getOrdersByUser(Request $request)
    {

        $orders = Order::where('user_id', $request->user_id)->orderBy('id', 'DESC')->paginate(5);


        foreach ($orders as $order) {

            $order['created_at_short'] = date('d/m/Y', strtotime($order->created_at));

            $order->status = Order::getStatusOrderSpanish($order->status, User::CLIENT);

        }

        return Response::json(array('success' => $orders), 200);

    }


    public function find(Request $request)
    {

        if ($request->has(['user_id', 'order_id'])) {


            $order = Order::where('id', ($request->input('order_id')))
                ->where('user_id', $request->input('user_id'))
                ->first();

            if ($order) {

                $order->item_qty = json_decode($order->item_qty);
                $order->payment_method = json_decode($order->payment_method);
                $order->user = json_decode($order->user);
                $order->shipping = json_decode($order->shipping);
                $order->direction = json_decode($order->direction);
                $order->billing = json_decode($order->billing);

                $order->direction->state = Estates::find($order->direction->estate_id);

                $order->status = Order::getStatusOrderSpanish($order->status, User::CLIENT);

                return Response::json(array('success' => $order), 200);
            }
            return Response::json(array('error' => 'Orden no encontrada'), 400);
        }
        return Response::json(array('error' => 'Faltan paramétros'), 400);
    }


    public function search(Request $request)
    {

        $search = $request->search;
        $filterStatus = $request->filterStatus;



        $orders = Order::where('order_cva', 'like', "%$search%")
            ->Orwhere('created_at', 'like', "%$search%")
            ->Orwhere('id', 'like', "%$search%");

        if ($filterStatus != "") {
            $orders->status($filterStatus);
        }


        $orders = $orders->paginate(10);


        if ($orders) {

            foreach ($orders as $order) {

                $order->item_qty = json_decode($order->item_qty);
                $order->payment_method = json_decode($order->payment_method);
                $order->user = json_decode($order->user);
                $order->shipping = json_decode($order->shipping);
                $order->direction = json_decode($order->direction);
                $order->billing = json_decode($order->billing);

                $order->direction->state = Estates::find($order->direction->estate_id);



                $order->status = Order::getStatusOrderSpanish($order->status, User::CLIENT);




            }

            return Response::json(array('success' => $orders), 200);
        }


        return Response::json(array('error' => "Orden no encontrada"), 400);
    }


    public function setShipping(Request $request)
    {


        $validator = Validator::make($request->all(),
            array(
                'numero_rastreo' => 'required',
                'compania' => 'required',
                'url_rastreo' => 'required',
                'order_id' => 'required|integer',
            )
        );

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }


        $order = Order::find($request->order_id);

        if ($order) {

            $order->shipping = json_decode($order->shipping);

            $shipping = json_encode([
                'shippingPrice' =>    $order->shipping->shippingPrice,
                'tracking_number' => $request->numero_rastreo,
                'company_shipping' => $request->compania,
                'url_traking' => $request->url_rastreo,
            ]);

            $order->shipping = $shipping;

            //actualizar estado
            $order->status = Order::status[7];

            $order->save();

            //enviar correo al cliente
            Mail::to(json_decode($order->user)->email)->send(new changeStatusOrder($order));

            return Response::json(array('success' => true), 200);
        }

        return Response::json(array('error' => 'error al actualizar'), 400);


    }


    public function finish_order(Request $request)
    {
        $validator = Validator::make($request->all(),
            array('order_id' => 'required|integer')
        );

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }


        $order = Order::find($request->order_id);

        if ($order) {

            //actualizar estado
            $order->status = Order::status[8];

            $order->save();

            return Response::json(array('success' => true), 200);
        }

        return Response::json(array('error' => 'error al actualizar'), 400);

    }


    public function checkStatus(Request $request)
    {
        // si el pedido no a sido respondido
        if ($request->has('id')) {

            $order_id = $request->input('id');

            $order = Order::find($order_id);

            if ($order) {

                if($order->status == 'pending'){
                    $order->status = Order::status[2];
                    $order->save();
                }

                return Response::json(array('success' => $order->status), 200);
            }

            return Response::json(array('success' => 'Sin orden de compra'), 400);
        }

        return Response::json(array('error' => 'Faltan parametros'), 400);

    }


    public function sendEmailTest(Request $request)
    {

        if ($request->has(['order_id', 'email'])) {


            //   $order = Order::find($request->order_id);//Pedido a CVA


            //  if (Order::createOrderCVA($order)) {
            $emailUser = ['jorge_manzano24@hotmail.com', 'luis.enrique.sesma@gmail.com'];
            // Enviar correo a user
            Mail::to($emailUser)->send(new NotifyCustomerPurchase($request->order_id));

            // Enviar correo a admin
            //    Mail::to($emailUser)->send(new NotifyPurchaseAdministrators($request->order_id));
            //   }


            return Response::json(array('success' => 'ok'), 200);

        }


        return Response::json(array('error' => 'Faltan parametros'), 400);

    }


}
