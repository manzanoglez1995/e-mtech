<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\CreditCard;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserCreditCardController extends Controller
{


    public function create(Request $request)
    {

        $validator = Validator::make($request->all(),
            array(
                'numeroDeTarjeta' => 'required|string|min:16|max:19',
                'nombre' => 'required|string|min:3',
                'mes' => 'required|int',
                'año' => 'required|int',
                'cvc' => 'required|string|min:3|max:3',
                'tipo' => 'required|string|min:2',
                'user_id' => 'required|integer',
            )
        );

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }


        $user = User::find($request->input('user_id'));

        if ($user) {

            $creditCard = CreditCard::create(
                array(
                    'number'=> str_replace(' ', '', $request->input('numeroDeTarjeta')),
                    'name' => strtoupper($request->input('nombre')),
                    'expireMonth'=> $request->input('mes'),
                    'expireYear'=> $request->input('año'),
                    'type'=> $request->input('tipo'),
                    'user_id' => $request->input('user_id'),

                ));

            if ($creditCard) {

                $creditCard->number = substr($creditCard->number, 12, 4);

                return Response::json(array('success' => $creditCard), 200);
            }
        }
        return Response::json(array('error' => 'No se logro guardar , intentalo mas tarde'), 400);
    }

    public function show($user_id)
    {
        $creditCards = CreditCard::where('user_id', $user_id)->get();

        if ($creditCards) {

            foreach ($creditCards as $creditCard){
                $creditCard->number = substr($creditCard->number, 12, 4);
            }

            return Response::json(array('success' => $creditCards), 200);
        }
        return Response::json(array('success' => 'Sin resultados'), 200);
    }



    public function destroy($creditCard)
    {
        $creditCard = CreditCard::find($creditCard);

        if ($creditCard) {

            $creditCard->delete();

            return Response::json(array('success' => 'exito'), 200);
        }
        return Response::json(array('error' => 'No encontrado'), 400);
    }
}
