<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ExtraCharge;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ExtraChargeController extends Controller
{


    public function show()
{
    $extraCharge = ExtraCharge::orderBy('updated_at', 'DESC')->paginate(10);

    return Response::json(array('success' => $extraCharge), 200);
}

    public function showActives()
    {
        $extraCharge = ExtraCharge::where('status',true)->orderBy('group', 'ASC')->get();

        return Response::json(array('success' => $extraCharge), 200);
    }

    public function delete($ExtraCharge_id){


        $ExtraCharge = ExtraCharge::find($ExtraCharge_id);

        if ($ExtraCharge) {

            $ExtraCharge->status = false;

            $ExtraCharge->save();

            return Response::json(array('success' => 'OK'), 200);
        }

        return Response::json(array('error' => "Usuario no encontrado"), 400);

    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255'],
            'correo' => ['required', 'string', 'email', 'max:255', 'unique:ExtraCharges,email'],
            'contraseña' => ['required', 'min:6'],
            'confirmar_contraseña' => ['required', 'min:6', 'same:contraseña'],

        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $ExtraCharge = ExtraCharge::create([
            'name' => $request->nombre,
            'email' => $request->correo,
            'password' => Hash::make($request->contraseña),
        ]);


        $token = $ExtraCharge->createToken('Personal Access Client')->accessToken;


        if ($ExtraCharge) {

            return Response::json(array('success' => ['ExtraCharge' => $ExtraCharge, 'api_token' => $token]), 200);

        } else {
            return Response::json(array('error' => "Fallo al registrar intentalo mas tarde"), 400);
        }

    }

    public function update(Request $request, $id)
    {



        $ExtraCharge = ExtraCharge::find($id);


        if ($ExtraCharge) {

            if ($request->has(['commission_price','shipping_price','status'])) {


                $validator = Validator::make($request->all(),
                    array(
                        'commission_price' => 'required',
                        'shipping_price' => 'required',
                        'status' => 'required'
                    ));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $ExtraCharge->commission_price = $request->commission_price;
                $ExtraCharge->shipping_price = $request->shipping_price;

                $ExtraCharge->status = $request->status == "true" ?  true:false;

                $ExtraCharge->save();

                return Response::json(array('success' => $ExtraCharge), 200);
            }

            return Response::json(array('error' => "Error al actualizar intentalo mas tarde"), 400);

        }

        return Response::json(array('error' => "Grupo no encontrado"), 400);

    }

    public function search(Request $request)
    {

        $search = $request->search;

        $extraCharge = ExtraCharge::where('group', 'like', "%$search%")->paginate(10);


        return Response::json(array('success' => $extraCharge), 200);

    }

}
