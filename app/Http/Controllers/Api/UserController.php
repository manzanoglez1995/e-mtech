<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\contactMessage;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller{


    public function __construct()
    {

        $this->middleware(['auth:api'])
            ->only([
                'logout',
                'AuthInfo'
            ]);
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nombre' => ['required', 'string', 'max:255'],
            'correo' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'contraseña' => ['required', 'min:6'],
            'confirmar_contraseña' => ['required', 'min:6', 'same:contraseña'],

        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        $user = User::create([
            'name' => $request->nombre,
            'email' => $request->correo,
            'password' => Hash::make($request->contraseña),
        ]);


        $token = $user->createToken('Personal Access Client')->accessToken;


        if ($user) {

            return Response::json(array('success' => ['user' => $user, 'api_token' => $token]), 200);

        } else {
            return Response::json(array('error' => "Fallo al registrar intentalo mas tarde"), 400);
        }

    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required'],
        ]);
        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }


        $user = User::where('email', Input::get('email'))->first();

        if ($user) {


            if (Hash::check(Input::get('password'), $user->password)) {


                $token = $user->createToken('Personal Access Client')->accessToken;

                return Response::json(array('success' => ['user' => $user, 'api_token' => $token]), 200);
            }
        }

        return Response::json(array('error' => "Usuario/contraseña incorrecta", 'exito' => false), 400);
    }

    public function logout()
    {
        $accessToken = Auth::user()->token();

        $accessToken->revoke();

        return Response::json(array('success' => "ok"), 200);
    }

    public function AuthInfo()
    {
        return Response::json(array('success' => Auth::user()), 200);
    }

    public function users()
    {
        $users = User::orderBy('created_at', 'DESC')->paginate(5);

        foreach ($users as $user) {

            if (Storage::disk('public')->exists($user->image)) {
                $user->image = asset('storage/') . '/' . $user->image;
            } else {
                $user->image = asset('admin/img/default-user.svg');
            }
        }
        return Response::json(array('success' => $users), 200);
    }

    public function search(Request $request)
    {

        $search = $request->search;

        $users = User::where('name', 'like', "%$search%")
            ->Orwhere('email', 'like', "%$search%")
            ->Orwhere('password', 'like', "%$search%")
            ->Orwhere('created_at', 'like', "%$search%")
            ->orderBy('created_at', 'DESC')
            ->paginate(5);

        foreach ($users as $user) {

            if (Storage::disk('public')->exists($user->imagen)) {
                $user->image = asset('storage') . '/' . $user->imagen;
            } else {
                $user->image = asset('admin/img/default-user.svg');
            }

        }

        return Response::json(array('success' => $users), 200);

    }

    public function update(Request $request, $id)
    {

        $user = User::find($id);


        if ($user) {

            if ($request->has('nombre')) {

                $validator = Validator::make($request->all(), array('nombre' => 'required'));
                if ($validator->fails()) {


                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $user->name = $request->nombre;
            }


            if ($request->has('correo')) {

                $validator = Validator::make($request->all(), array('correo' => 'required'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $user->email = $request->correo;
            }

            if ($request->has('telefono')) {

                $validator = Validator::make($request->all(), array('telefono' => 'required|max:15'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $user->telephone = $request->telefono;
            }

            if ($request->has('celular')) {

                $validator = Validator::make($request->all(), array('celular' => 'required|max:15'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $user->cellphone = $request->celular;
            }

            if ($request->has(['contraseña', 'confirmar_contraseña', 'contraseña_actual'])) {

                if (Hash::check($request->input("contraseña_actual"), $user->password)) {

                    $validator = Validator::make($request->all(), array(
                        'contraseña' => 'required',
                        'confirmar_contraseña' => 'required'));
                    if ($validator->fails()) {
                        return Response::json(array('error' => $validator->errors()), 422);
                    }

                    if ($request->contraseña === $request->confirmar_contraseña) {

                        $user->password = Hash::make($request->contraseña);
                    } else {

                        return Response::json(array('error' => array(
                            'confirmar_contraseña' => ["La nueva contraseña no coincide"]
                        )), 422);
                    }
                } else {
                    return Response::json(array('error' => array(
                        'contraseña_actual' => ["La contraseña actual no coincide"]
                    )), 422);
                }

            }

            if ($request->has('imagen')) {

                if (Storage::disk('public')->exists($user->imagen))
                    Storage::disk('public')->delete($user->imagen);

                $user->imagen = $request->file('imagen')->store('Users_img');

            }

            $user->save();


            return Response::json(array('data' => $user), 200);


        } else {
            return Response::json(array('error' => "Usuario no encontrado"), 400);


        }
    }

    public function delete($user_id)
    {

        $user = User::find($user_id);
        if ($user) {

            if (Storage::disk('public')->exists($user->image))
                Storage::disk('public')->delete($user->image);


            $userTokens = $user->tokens;

            foreach ($userTokens as $token) {
                $token->revoke();
            }


            // borrar toda la infomacion

            foreach ($user->billing as $billing) {
                $billing->delete();
            }
            foreach ($user->shippingAdress as $shippingAdress) {
                $shippingAdress->delete();
            }
            foreach ($user->creditCards as $creditCard) {
                $creditCard->delete();
            }


            $user->delete();


            return Response::json(array('success' => 'OK'), 200);
        }

        return Response::json(array('error' => "Usuario no encontrado"), 400);

    }

    public function changeRole(Request $request)
    {

        $user = User::find($request->user_id);
        if ($user) {


            if ($user->role == User::CLIENT) {

                $user->role = User::ADMIN;
            } else {

                $user->role = User::CLIENT;
            }


            $user->save();


            return Response::json(array('success' => 'OK'), 200);
        }

        return Response::json(array('error' => "Usuario no encontrado"), 400);
    }


    public function contactMessage(Request $request)
    {



        $emailUser = ['jorge_manzano24@hotmail.com', 'luis.enrique.sesma@gmail.com'];
        // Enviar correo a user
        Mail::to($emailUser)->send(new contactMessage($request->all()));


        return Response::json(array('success' => $request->all()), 200);


    }


}
