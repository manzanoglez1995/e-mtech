<?php

namespace App\Http\Controllers\Api;

use App\Models\ExtraCharge;
use App\Models\HelperSearch;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use MarkWilson\XmlToJson\XmlToJsonConverter;


class ProductController extends Controller
{


    function show()
    {
        $products = Product::hasAvailableMin(2)->paginate(12);

        foreach ($products as $product) {

            Product::priceWithExtraCharge($product);

        }


        return Response::json(array('success' => $products), 200);

    }

    function findByKey(Request $request)
    {

        if ($request->has('key')) {

            $product = Product::where('key', $request->input('key'))->first();

            if ($product) {

                Product::priceWithExtraCharge($product);

                return Response::json(array('success' => $product), 200);

            }

            return Response::json(array('error' => 'Producto no encontrado'), 400);
        }
        return Response::json(array('error' => 'Faltan parametros'), 400);
    }


    function quoteCart(Request $request)
    {


        if ($request->has('ShopCart')) {

            $products = json_decode($request->input('ShopCart'));

            $products_items = array();
            $total = 0.0;
            $iva = 0.0;
            $shipping_price = 0.0;
            $countItems = 0;


            foreach ($products as $product) {

                $cartItem = Product::where('key', $product->item)->first();

                if ($cartItem) {

                    //agrega cargos extras
                    Product::priceWithExtraCharge($cartItem);

                    // calcula el subtotal y el total de cva
                    $iva += $cartItem->iva * $product->quantity;

                    $subtotal = $cartItem->price * $product->quantity;
                    $total += $subtotal;

                    // agrego propiedad a cada producto
                    $cartItem['shoppingCart'] = array("subtotal" => $subtotal, "quantity" => $product->quantity);

                    //cuantos items hay en el carrito
                    $countItems += $product->quantity;

                    // suma el precio de envio
                    $shipping_price += $cartItem->shipping_price * $product->quantity;

                    array_push($products_items, $cartItem);
                }
            }


            return Response::json(array('success' => [
                "Product" => $products_items,
                "quantityItem" => $countItems,
                'shipping_price' => $shipping_price,
                'iva' => Product::formatPrice($iva),
                'subtotal' => Product::calcularteSubtotal($total, $iva),
                'total' => Product::totalPrice($total + $shipping_price),
            ]), 200);

        }

        return Response::json(array('error' => 'error al obtener los productos'), 400);

    }


    function search(Request $request)
    {

        if ($request->has('searchText')) {

            $search = $request->input('searchText');

            $brand = $request->input('brand');
            $group = $request->input('group');
            $min = $request->input('min');
            $max = $request->input('max');


            //grupo_no,marca_no, texto_no -
            if (!$group && !$brand && !$search) {
                $products = Product::hasAvailableMin(2);
            }

            //grupo_no,marca_no, texto_si -
            if (!$group && !$brand && $search) {

                $keyword = HelperSearch::where('keyword', 'like', "%$search%")->first();


                if ($keyword) {

                    $products = Product::group($keyword->group)->hasAvailableMin(2);

              /*      $products = Product::where("key", "LIKE", "%$search%")->group($keyword->group,"like")->hasAvailableMin(2)
                        ->OrWhere("manufacturer_code", "LIKE", "%$search%")->group($keyword->group,"like")->hasAvailableMin(2)
                        ->OrWhere("description", "LIKE", "%$search%")->group($keyword->group,"like")->hasAvailableMin(2)
                        ->OrWhere("brand", "LIKE", "%$search%")->group($keyword->group,"like")->hasAvailableMin(2);
*/

                } else {

                    $productsOnGroup = Product::group($search)->hasAvailableMin(2)->count();

                    if ($productsOnGroup >= 1) {

                        $products = Product::group($search)->hasAvailableMin(2);

                    } else {

                        $productsOnDescription = Product::description($search)->hasAvailableMin(2)->count();

                        if ($productsOnDescription >= 1) {

                            $products = Product::description($search)->hasAvailableMin(2);

                        }else{

                            $products = Product::where("key", "LIKE", "%$search%")->hasAvailableMin(2)
                                ->OrWhere("manufacturer_code", "LIKE", "%$search%")->hasAvailableMin(2)
                                ->OrWhere("description", "LIKE", "%$search%")->hasAvailableMin(2)
                                ->OrWhere("brand", "LIKE", "%$search%")->hasAvailableMin(2)
                                ->OrWhere("group", "LIKE", "%$search%")->hasAvailableMin(2);
                        }
                    }
                }
            }


            //grupo_si,marca_no, texto_no -
            if ($group && !$brand && !$search) {
                $products = Product::group($group)->where('available', '>=', '2');
            }

            //grupo_si,marca_no, texto_si -
            if ($group && !$brand && $search) {
                $products = Product::where("key", "LIKE", "%$search%")->group($group)->hasAvailableMin(2)
                    ->OrWhere("manufacturer_code", "LIKE", "%$search%")->group($group)->hasAvailableMin(2)
                    ->OrWhere("description", "LIKE", "%$search%")->group($group)->hasAvailableMin(2)
                    ->OrWhere("brand", "LIKE", "%$search%")->group($group)->hasAvailableMin(2);
            }

            //grupo_si,marca_si, texto_no -
            if($group && $brand  && !$search){
                $products = Product::group($group)->brand($brand)->hasAvailableMin(2);
            }

            //grupo_si,marca_si, texto_si -
            if ($group && $brand && $search) {

                $products = Product::where("key", "LIKE", "%$search%")->group($group)->brand($brand)->hasAvailableMin(2)
                    ->OrWhere("manufacturer_code", "LIKE", "%$search%")->group($group)->brand($brand)->hasAvailableMin(2)
                    ->OrWhere("description", "LIKE", "%$search%")->group($group)->brand($brand)->hasAvailableMin(2);
            }

            //grupo_no,marca_si, texto_si +
            if (!$group && $brand && $search) {

                $products = Product::where("key", "LIKE", "%$search%")->brand($brand)->hasAvailableMin(2)
                    ->OrWhere("manufacturer_code", "LIKE", "%$search%")->brand($brand)->hasAvailableMin(2)
                    ->OrWhere("description", "LIKE", "%$search%")->brand($brand)->hasAvailableMin(2)
                    ->OrWhere("brand", "LIKE", "%$search%")->brand($brand)->hasAvailableMin(2);
            }

            //grupo_no,marca_si, texto_no +
            if (!$group && $brand && !$search) {
                $products = Product::brand($brand)->hasAvailableMin(2);
            }


            if ($min && $max) {
                $products->price($min, $max);
            }

            $products = $products->paginate(12);

            foreach ($products as $product) {

                Product::priceWithExtraCharge($product);
            }

            return Response::json(array('success' => $products), 200);

        }

        return Response::json(array('error' => 'no enviaste texto de busqueda'), 422);


    }


    function search_filter(Request $request)
    {


        if ($request->has('searchText')) {

            $search = $request->input('searchText');

            $brand = $request->input('brand');
            $group = $request->input('group');
            $min = $request->input('min');
            $max = $request->input('max');


            if (!empty($group)) {

                $products = Product::group($group)->where('available', '>=', '2');


            } else {


                $keyword = HelperSearch::where('keyword', 'like', "%$search%")->first();

                $keyword = is_null($keyword) ? $search : $keyword->group;

                $products = Product::group($keyword, "like")->where('available', '>=', '2');

            }


            if (!empty($brand)) {

                $products->brand($brand);

            } else {

                $products->brand($search, "like");

            }


            $products->key($search)->where('available', '>=', '2');

            $products->manufacturerCode($search)->where('available', '>=', '2');

            $products->description($search)->where('available', '>=', '2');


            if ($min || $max) {
                $products->price($min, $max);
            }


            $products = $products->paginate(12);


            foreach ($products as $product) {

                Product::priceWithExtraCharge($product);
            }

            return Response::json(array('success' => $products), 200);

        }

        return Response::json(array('error' => 'no enviaste texto de busqueda'), 422);


    }


    function list_brands()
    {


        $url = "http://www.grupocva.com/catalogo_clientes_xml/marcas.xml";

        $content = utf8_encode(file_get_contents($url));
        $brands = simplexml_load_string($content);

        return Response::json(
            array('success' => $brands), 200);
    }


    function synchronize()
    {

        try{

        
        $url = Product::URL . "cliente=" . Product::CLIENT . "&MonedaPesos=1&exist=3";

        $xml = file_get_contents($url);

        $string = trim(preg_replace('/\s+/', ' ', $xml));

        if($string===""){

            return Response::json(array('error' => ["mensaje" => "Sin resultados encontrados"]), 404);
        }

        foreach ($xml as $x) {

            $product = Product::where('key', $x->clave)->first();

            if (!$product) {
                $product = new Product();
            }

            $product->key = $x->clave;
            $product->manufacturer_code = $x->codigo_fabricante;
            $product->description = $x->descripcion;
            $product->principal = $x->principal;
            $product->group = $x->grupo;
            $product->brand = $x->marca;
            $product->warranty = $x->garantia;
            $product->class = $x->clase;
            $product->available = $x->disponible;
            $product->price = $x->precio;
            $product->currency = $x->moneda;
            $product->data_sheet = $x->ficha_tecnica;
            $product->bussiness_card = $x->ficha_comercial;
            $product->image = $x->imagen;
            $product->AvailableCD = $x->disponibleCD;

            $product->save();

        }

        $totalXml = count($xml);

        return Response::json(array('success' => ["synchronized" => $totalXml]), 200);
    }catch(\Exception $e){

        return Response::json(array('error' => ["mensaje" => (String)$xml]), 400);
    }
    }


    //Admin Service

    function products()
    {
        $rows = Product::orderBy('created_at', 'DESC')->paginate(10);

        return Response::json(array('success' => $rows), 200);
    }


    function simpleSearch(Request $request)
    {

        if ($request->has('search')) {

            $search = $request->input('search');

            $keyword = HelperSearch::Where('keyword', 'like', "%$search%")->first();

            $keyword = is_null($keyword) ? $search : $keyword->group;

            $products = Product::where('brand', 'like', "%$search%")->where('available', '>=', '2')
                ->OrWhere('key', 'like', "%$search%")->where('available', '>=', '2')
                ->OrWhere('description', 'like', "%$search%")->where('available', '>=', '2')
                ->OrWhere('manufacturer_code', 'like', "%$search%")->where('available', '>=', '2')
                ->OrWhere('group', 'like', "%$keyword%")->where('available', '>=', '2')
                ->OrWhere('group', 'like', "%$search%")->where('available', '>=', '2')
                ->paginate(10);


            foreach ($products as $product) {
                Product::priceWithExtraCharge($product);
            }

            return Response::json(array('success' => $products), 200);


        }

        return Response::json(array('error' => 'no enviaste texto de busqueda'), 422);

    }


    function mostSelledProducts()
    {

        $orders = Order::all();

        $products = array();

        //obtengo todos los productos comprados
        if ($orders) {

            foreach ($orders as $order) {

                foreach (json_decode($order->item_qty) as $order_item) {

                    $key = $order_item->item;
                    $qty = $order_item->quantity;

                    $inArray = false;

                    //Agrupo todos los productos iguales
                    if (isset($products[$key])) {

                        $products[$key] = $products[$key] += $qty;
                        $inArray = true;
                    }

                    if (!$inArray) {
                        $products[$key] = $qty;
                    }
                }
            }
        }

        //Ordena de mayor a menor
        arsort($products);

        $JsonProducts = array();

        foreach ($products as $productQty) {

            $item = Product::where('key', key($products))->first();

            if ($item) {

                $item['sold'] = $productQty;

                $JsonProducts[] = $item;
            }

            next($products);
        }


        return Response::json(array('success' => $JsonProducts), 200);

    }

    function getSearchKeyword(Request $request)
    {

        if ($request->has('group')) {

            $keyword = HelperSearch::where('group', $request->input('group'))->get();

            return Response::json(array('success' => $keyword), 200);
        }

        return Response::json(array('error' => 'Sin grupo seleccionado'), 422);
    }

    function addSearchKeyword(Request $request)
    {

        if ($request->has(['group', 'keyword'])) {

            $keyword = new HelperSearch();

            $keyword->group = $request->input('group');
            $keyword->keyword = $request->input('keyword');
            $keyword->save();

            return Response::json(array('success' => true), 200);
        }

        return Response::json(array('error' => 'Sin grupo seleccionado'), 422);
    }

    function destroySearchKeyword(Request $request)
    {

        if ($request->has('keyword_id')) {

            HelperSearch::destroy($request->input('keyword_id'));

            return Response::json(array('success' => true), 200);
        }

        return Response::json(array('error' => 'Sin palabra seleccionado'), 422);
    }


}
