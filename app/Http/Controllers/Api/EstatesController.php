<?php

namespace App\Http\Controllers\Api;

use App\Models\Estates;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class EstatesController extends Controller
{

    public function show(){

        $estates = Estates::all();
        return Response::json(array('success' => $estates), 200);
    }

}
