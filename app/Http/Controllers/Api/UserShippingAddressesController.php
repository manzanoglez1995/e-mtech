<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\UserShippingAddresses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserShippingAddressesController extends Controller
{

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(),
            array(
                'nombreUbicacion' => 'required|string|min:3',
                'calle' => 'required|string|min:3',
                'numeroExterior' => 'required|string|min:1',
                'numeroInterior' => 'min:1',
                'referencia' => 'string|min:6',
                'colonia' => 'required|string|min:3',
                'codigoPostal' => 'required|string',
                'ciudad' => 'required|string',
                'estado' => 'required|string',
                'user_id' => 'required|integer',
            )
        );

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }


        $user = User::find($request->input('user_id'));

        if ($user) {


            $UserShippingAddresses = new UserShippingAddresses();

            $UserShippingAddresses->name = $request->input('nombreUbicacion');
            $UserShippingAddresses->street = $request->input('calle');
            $UserShippingAddresses->external_number = $request->input('numeroExterior');

            if ($request->input('numeroInterior') != "") {
                $UserShippingAddresses->internal_number = $request->input('numeroInterior');
            }
            if ($request->input('referencia') != "") {
                $UserShippingAddresses->reference = $request->input('referencia');
            }
            $UserShippingAddresses->colony = $request->input('colonia');
            $UserShippingAddresses->cp = $request->input('codigoPostal');
            $UserShippingAddresses->city = $request->input('ciudad');
            $UserShippingAddresses->estate_id = $request->input('estado');
            $UserShippingAddresses->country = 'México';
            $UserShippingAddresses->user_id = $request->input('user_id');
            $UserShippingAddresses->active = true;

            $UserShippingAddresses->save();

            if ($UserShippingAddresses) {
                $UserShippingAddresses->state;

                return Response::json(array('success' => $UserShippingAddresses), 200);
            }
        }
        return Response::json(array('error' => 'No se logro guardar , intentalo mas tarde'), 400);
    }

    public function show($user_id)
    {


        $UserShippingAddresses = UserShippingAddresses::where('user_id', $user_id)
            ->where('active', true)
            ->get();

        if ($UserShippingAddresses) {


            foreach ($UserShippingAddresses as $adress){
                $adress->state;
            }

            return Response::json(array('success' => $UserShippingAddresses), 200);
        }
        return Response::json(array('success' => 'Sin resultados'), 200);
    }

    public function update(Request $request, $id)
    {


        $UserShippingAddresses = UserShippingAddresses::find($id);


        if ($UserShippingAddresses) {

            if ($request->has('nombreUbicacion')) {

                $validator = Validator::make($request->all(),
                    array('nombreUbicacion' => 'string|min:3'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }


                $UserShippingAddresses->name = strtoupper($request->nombreUbicacion);
            }

            if ($request->has('colonia')) {

                $validator = Validator::make($request->all(),
                    array('colonia' => 'string|min:3',));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $UserShippingAddresses->colony = $request->colonia;
            }

            if ($request->has('calle')) {

                $validator = Validator::make($request->all(),
                    array('calle' => 'string|min:3',));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $UserShippingAddresses->street = $request->calle;
            }

            if ($request->has('numeroExterior')) {

                $validator = Validator::make($request->all(),
                    array('numeroExterior' => 'string|min:3',));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $UserShippingAddresses->external_number = $request->numeroExterior;
            }

            if ($request->has('numeroInterior')) {

                $validator = Validator::make($request->all(),
                    array('numeroInterior' => 'min:1',));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $UserShippingAddresses->internal_number = $request->numeroInterior;
            }

            if ($request->has('referencia')) {

                $validator = Validator::make($request->all(),
                    array('referencia' => 'string|min:6'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $UserShippingAddresses->reference = $request->referencia;
            }

            if ($request->has('codigoPostal')) {

                $validator = Validator::make($request->all(),
                    array('codigoPostal' => 'string'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $UserShippingAddresses->cp = $request->codigoPostal;
            }

            if ($request->has('ciudad')) {

                $validator = Validator::make($request->all(),
                    array('ciudad' => 'string'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $UserShippingAddresses->city = $request->ciudad;
            }

            if ($request->has('estado')) {

                $validator = Validator::make($request->all(),
                    array('estado' => 'integer'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $UserShippingAddresses->estate_id = $request->estado;
            }

            $UserShippingAddresses->save();

            $UserShippingAddresses->state;

            return Response::json(array('success' => $UserShippingAddresses), 200);
        }
        return Response::json(array('error' => 'No encontrado'), 400);
    }

    public function destroy($UserShippingAddresses)
    {
        $UserShippingAddresses = UserShippingAddresses::find($UserShippingAddresses);

        if ($UserShippingAddresses) {

            $UserShippingAddresses->active = false;

            $UserShippingAddresses->save();

            return Response::json(array('success' => 'exito'), 200);
        }
        return Response::json(array('error' => 'No encontrado'), 400);
    }
}
