<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\UserBilling;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserBillingController extends Controller
{


    public function create(Request $request)
    {

        $validator = Validator::make($request->all(),
            array(
                'rfc' => 'required|string|min:12',
                'razonSocial' => 'required|string|min:6',
                'user_id' => 'required|integer',
            )
        );

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }


        $validatenameOrSocialReason = UserBilling::where('nameOrSocialReason', $request->input('razonSocial'))
            ->where('active', true)
            ->first();

        if ($validatenameOrSocialReason) {
            return Response::json(array('error' => array(
                'razonSocial'=>['El campo razon social ya ha sido registrado.'])), 422);
        }

        $validatenameRFC= UserBilling::where('rfc', $request->input('rfc'))
            ->where('active', true)
            ->first();

        if ($validatenameRFC) {
            return Response::json(array('error' => array(
                'RFC'=> ['El campo RFC ya ha sido registrado.'])), 422);
        }



        $user = User::find($request->input('user_id'));


        if ($user) {


            $userBillings = UserBilling::create(
                array(
                    'RFC' => strtoupper($request->input('rfc')),
                    'nameOrSocialReason' => $request->input('razonSocial'),
                    'user_id' => $request->input('user_id'),
                    'active' => true,
                ));

            if ($userBillings) {
                return Response::json(array('success' => $userBillings), 200);
            }
        }
        return Response::json(array('error' => 'No se logro guardar , intentalo mas tarde'), 400);
    }

    public function show($user_id)
    {
        //  dd(Auth::user());

        $userBillings = UserBilling::where('user_id', $user_id)
            ->where('active', true)
            ->get();

        if ($userBillings) {
            return Response::json(array('success' => $userBillings), 200);
        }
        return Response::json(array('success' => 'Sin resultados'), 200);
    }

    public function update(Request $request, $id)
    {


        $userBilling = UserBilling::find($id);


        if ($userBilling) {

            if ($request->has('rfc')) {

                $validator = Validator::make($request->all(),
                    array('rfc' => 'string|min:12',));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $validatenameRFC= UserBilling::where('rfc', $request->input('rfc'))
                    ->where('active', true)
                    ->where('id','!=',$id)
                    ->first();

                if ($validatenameRFC) {
                    return Response::json(array('error' => array(
                        'RFC'=> ['El campo RFC ya ha sido registrado.'])), 422);
                }


                $userBilling->RFC = strtoupper($request->rfc);
            }

            if ($request->has('razonSocial')) {

                $validator = Validator::make($request->all(),
                    array('razonSocial' => 'string|min:6',));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors()), 422);
                }

                $validatenameOrSocialReason = UserBilling::where('nameOrSocialReason', $request->input('razonSocial'))
                    ->where('active', true)
                    ->where('id','!=',$id)
                    ->first();


                if ($validatenameOrSocialReason) {
                    return Response::json(array('error' => array(
                        'razonSocial'=>['El campo razon social ya ha sido registrado.'])), 422);
                }



                $userBilling->nameOrSocialReason = $request->razonSocial;
            }

            $userBilling->save();

            return Response::json(array('success' => $userBilling), 200);
        }
        return Response::json(array('error' => 'No encontrado'), 400);
    }

    public function destroy($userBilling)
    {
        $userBilling = UserBilling::find($userBilling);

        if ($userBilling) {

            $userBilling->active = false;

            $userBilling->save();

            return Response::json(array('success' => 'exito'), 200);
        }
        return Response::json(array('error' => 'No encontrado'), 400);
    }
}
