<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ShopController extends Controller
{

    public function home()
    {
        return view('shop.home')->withPage('home');
    }

    public function shop()
    {
        return view('shop.shop')->withPage('shop');
    }

    public function contact()
    {
        return view('shop.contact')->withPage('contact');
    }

    public function cart()
    {
        return view('shop.cart.detailCart')->withPage('cart');


    }

    public function checkout()
    {

        if(Session::has('error')){
            $message = Session::pull('error', null);
            return view('shop.payment.checkout')
                ->with('type','error')
                ->with('message',$message)
                ->withPage('checkout');

        }
        if(Session::has('success')){
            $message = Session::pull('success', null);
            return view('shop.payment.checkout')
                ->with('type','success')
                ->with('message',$message)
                ->withPage('checkout');

        }
            return view('shop.payment.checkout')->withPage('checkout');

    }

    public function detailProduct($key)
    {
        return view('shop.products.detailProduct', compact('key'))->withPage('detail-product')->withParameter($key);
    }

    public function account()
    {
        return view('shop.profile.account')->withPage('account');
    }

    public function cards()
    {
        return view('shop.profile.cards')->withPage('cards');
    }

    public function purchases()
    {
        return view('shop.profile.purchases')->withPage('purchases');
    }

    public function shippingInformation()
    {
        return view('shop.profile.shippingInformation')->withPage('shippingInformation');
    }

    public function orderDetail($id)
    {
        return view('shop.profile.orderDetail')->withPage('orderDetailShop')->withParameter($id)->with("id",$id);
    }

}
