<?php

namespace App\Http\Controllers\Shop;

use App\Mail\NotifyCustomerPurchase;
use App\Mail\NotifyPurchaseAdministrators;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Models\UserBilling;
use App\Models\UserShippingAddresses;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\InputFields;
use PayPal\Api\Payer;
use PayPal\Api\WebProfile;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;


class PaymentController extends Controller
{
    private $_api_context;

    public function __construct()
    {
        // setup PayPal api context
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payment(Request $request)
    {

        //Productos
        $products = json_decode($request->input('item_qty'));

        if (empty($products)) {
            return Response::json(array('error' => 'Debes tener productos en el carrito'), 400);
        }

        //Paypal
        try {

            $currency = 'MXN';


            $paypal = $this->paypalIntent($products, $currency);


            $paypal['intent']->create($this->_api_context);

            //Si  salio bien  busca el link de redireccion
            foreach ($paypal['intent']->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }


        } catch (\PayPal\Exception\PPConnectionException $ex) {

            return Response::json(array('error' => 'Ups! Algo salió mal'), 400);

        }


        //Calculo precio de envio
        $shipping = json_encode([
            'shippingPrice' => $paypal['shipping'],
            'tracking_number' => 00000000000000,
            'company_shipping' => 'N/A',
            'url_traking' => '',
        ]);

        //Calculo precio de envio
        $payment_method = json_encode([
            'method' => 'Paypal',
            'key' => $paypal['intent']->getId(),  //Paypal_id
        ]);


        $order = new Order();
        $emailUser = null;

        //Guardo la compra
        if (Auth::check()) {


            $address = UserShippingAddresses::find($request->input('address_id'));
            if (!$address) {
            $address = null;
            return Response::json(array('error' => 'Información de envio'), 400);
            }

            $billing = UserBilling::find($request->input('billing_id'));
            if (!$billing) {
                $billing = null;
                //   return Response::json(array('error' => 'Datos de facturación no validos'), 400);
            }


            if ($request->has(['rfc', 'razonSocial'])) {

                $billing = new UserBilling();

                $billing->RFC = $request->input('rfc');
                $billing->nameOrSocialReason = $request->input('razonSocial');

            }else{
                $billing = new UserBilling();

                $billing->RFC = "";
                $billing->nameOrSocialReason = "";
            }


            $order->user = Auth::user();

            $order->user_id = Auth::user()->id;

            $order->item_qty = $request->input('item_qty');
            $order->payment_method = $payment_method;
            $order->shipping = $shipping;
            $order->direction = $address;
            $order->billing = $billing;
            $order->total = $paypal['total'];
            $order->subtotal = $paypal['subtotal'];
            $order->vat = $paypal['iva'];
            $order->status = Order::status[0];

        } else {

            // modo invitado

            //Validar que vengan todos los datos para levantar el pedido
            $validator = Validator::make($request->all(), array(
                'nombre' => 'required| string| max:255',
                'correo' => 'required| string| email|max:255',
                'telefono' => 'required|numeric',
                'celular' => 'required|numeric',

                // 'razonSocial' => 'string|min:6',
                //  'rfc' => 'string|min:12',

                'colonia' => 'required|string|min:3',
                'calle' => 'required|string|min:3',
                'numeroExterior' => 'required|string|min:1',
                'codigoPostal' => 'required|string',
                'ciudad' => 'required|string',
                'estado' => 'required',
                'pais' => 'required|string',
                'referencia' => 'string|min:6',
            ));

            if ($validator->fails()) {
                return Response::json(array('error' => $validator->errors()), 422);
            }

            //Crea usuario invitado
            $user = new User();

            $user->name = $request->input('nombre');
            $user->email = $request->input('correo');
            $user->telephone = $request->input('telefono');
            $user->cellphone = $request->input('celular');

            //Crea datos de facturacion

            if ($request->has(['rfc', 'razonSocial'])) {

                $billing = new UserBilling();

                $billing->RFC = $request->input('rfc');
                $billing->nameOrSocialReason = $request->input('razonSocial');

            }else{
                $billing = new UserBilling();

                $billing->RFC = "";
                $billing->nameOrSocialReason = "";
            }


            //Crea direccion
            $adress = new UserShippingAddresses();

            $adress->colony = $request->input('colonia');
            $adress->street = $request->input('calle');
            $adress->external_number = $request->input('numeroExterior');
            $adress->internal_number = $request->input('numeroInterior');
            $adress->cp = $request->input('codigoPostal');
            $adress->city = $request->input('ciudad');
            $adress->estate_id = $request->input('estado');
            //    $adress->country = $request->input('pais'); por ahora solo mexico
            $adress->country = 'México';
            $adress->reference = $request->input('referencia');


            //Crear orden
            $order->user = $user;

            $order->user_id = 0;

            $order->item_qty = $request->input('item_qty');
            $order->payment_method = $payment_method;
            $order->shipping = $shipping;
            $order->direction = $adress;
            $order->billing = $billing;
            $order->total = $paypal['total'];
            $order->subtotal = $paypal['subtotal'];
            $order->vat = $paypal['iva'];
            $order->status = Order::status[0];

        }


        //Si se creo link de paypal
        if (isset($redirect_url)) {

            $order->save();

            return Response::json(array('success' => array(
                'order_id' => $order->id,
                'url' => $redirect_url,
            )), 200);
        }

        return Response::json(array('error' => 'Ups! Error desconocido.'), 400);
    }

    private function paypalIntent($products, $currency)
    {
        $subtotal = 0.0;
        $iva = 0.0;
        $shipping_price = 0.0;

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $items = array();


        foreach ($products as $product) {

            $cartItem = Product::where('key', $product->item)->first();


            //agrega cargos extras
            Product::priceWithExtraCharge($cartItem);

            // suma el precio de envio
            $shipping_price += $cartItem->shipping_price * $product->quantity;

            // calcula el subtotal y el total
            $subtotal += $cartItem->price * $product->quantity;

            $iva += $cartItem->iva * $product->quantity;

            //Crea items para paypal
            $item = new Item();

            $item->setName($cartItem->key)
                ->setCurrency($currency)
                ->setDescription($cartItem->description)
                ->setQuantity($product->quantity)
                ->setPrice($cartItem->price);

            $items[] = $item;
        }

        $item_list = new ItemList();
        $item_list->setItems($items);


        //Se Agrega Precio de envio
        $details = new Details();
        $details->setSubtotal($subtotal)->setShipping($shipping_price);


        //Quitar de paypal donde se opcion de envio
        $inputFields = new InputFields();
        $inputFields->setNoShipping(1);


        /*
         //Crear web profile
 //En caso de cambiar de cuenta , ejecutar solo una vez , si exite un perfil, usar la linea de abajo
         $webProfile = new WebProfile();
         $webProfile->setName("E-mtech");
         $webProfile->setInputFields($inputFields);
         $createProfileResponse = $webProfile->create($this->_api_context);
         $webProfile = WebProfile::get($createProfileResponse->getId(), $this->_api_context);
         dd($webProfile);
 */

        // obtiene el perfil de e-mtech para ocultar el metodo de envio de paypal
        $webProfile = WebProfile::get_list($this->_api_context)[0];


        //Calculo el subtotal  mas precio de envio
        $total = Product::totalPrice($subtotal + $shipping_price);


        //Preparamos compra en paypal
        $amount = new Amount();
        $amount->setCurrency($currency)
            ->setTotal($total)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Pedido E-mtech');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('paymentStatus'))
            ->setCancelUrl(URL::route('paymentStatus'));

        //configurar pago  a paypal
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction))
            ->setExperienceProfileId($webProfile->getId());


        return array(
            'total' => Product::formatPrice($total),
            'iva' => Product::formatPrice($iva),
            'subtotal' => Product::calcularteSubtotal($subtotal, $iva),
            'shipping' => Product::formatPrice($shipping_price),
            'intent' => $payment,
        );

    }

    public function getPaymentStatus()
    {

        //Si el pedido no a sido respondido
        if (Input::has(['paymentId', 'PayerID', 'token'])) {

            $payment_id = Input::get('paymentId');
            $payerId = Input::get('PayerID');
            $token = Input::get('token');


            $orders = Order::where('status', 'pending')->get();

            $order = null;

            foreach ($orders as $order_) {


                if (json_decode($order_->payment_method)->key == $payment_id) {
                    $order = $order_;
                    break;
                }
            }

            if (is_null($order)) {
                Session::flash('error', '¡Hubo un problema al intentar pagar con Paypal!.');
                return Redirect::route('checkout');
            }

            $payment = Payment::get($payment_id, $this->_api_context);

            $execution = new PaymentExecution();
            $execution->setPayerId($payerId);

            //Realizar el pago
            $result = $payment->execute($execution, $this->_api_context);

            if ($result->getState() == 'approved') { // pago realizado

                //update pedido a aprobado

                $order->status = Order::status[1];

                $order->save();

                $order_id = $order->id;

                $order = Order::find($order_id);//Pedido a CVA

                //Pedido a CVA

                    if (Order::createOrderCVA($order)) {

                        // Enviar correo a user
                        Mail::to(json_decode($order->user)->email)->send(new NotifyCustomerPurchase($order_id));

                        // Enviar correo a admin

                        $administrators = User::where('role', User::ADMIN)->get();

                        foreach ($administrators as $admin) {
                            $emailsAdm [] = $admin->email;
                        }

                        Mail::to($emailsAdm)->send(new NotifyPurchaseAdministrators($order_id));

                    }



                Session::flash('success', '¡Compra realizada!.');//elimina el carrito del lado del cliente
                return Redirect::route('checkout');

            } else {


                //en caso de error desconocido
                $order->status = Order::status[3];
                $order->save();


                Session::flash('error', '¡Hubo un problema al intentar pagar con Paypal!.');
                return Redirect::route('checkout');
            }


        }


        //En caso de cancelar
        Session::flash('error', '¡La compra fue cancelada!.');
        return Redirect::route('checkout');
    }


}
