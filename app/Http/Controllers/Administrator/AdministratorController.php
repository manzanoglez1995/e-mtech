<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Api\ProductController;
use App\Models\Estates;
use App\Models\ExtraCharge;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AdministratorController extends Controller
{

    public function dashboard()
    {
        return view('administrator.dashboard')->withPage('Dashboard');
    }

    public function users()
    {
        return view('administrator.users.table')->withPage('Usuarios');
    }

    public function products()
    {
        return view('administrator.products.table')->withPage('Productos');
    }

    public function editUsers($id)
    {

        $user = User::find($id);
        if ($user) {
            $user->billing;
            $user->shippingAdress;
            $user->creditCards;

            return view('administrator.users.show', compact('user'))->withPage('MostrarUsuarios')->withParameter($id);
        }


        return Redirect::route('users');
    }


    public function redirect()
    {
        return Redirect::route('orders');
    }

    public function detailOrders($id)
    {
        $order = Order::find($id);
        if ($order) {

            $order->item_qty = json_decode($order->item_qty);
            $order->payment_method = json_decode($order->payment_method);
            $order->user = json_decode($order->user);
            $order->shipping = json_decode($order->shipping);
            $order->direction = json_decode($order->direction);
            $order->billing = json_decode($order->billing);

            $order->direction->state = Estates::find($order->direction->estate_id);

            $order->status = Order::getStatusOrderSpanish($order->status,User::CLIENT);

            return view('administrator.orders.show', compact('order'))->withPage('Detalle')->withParameter($id);
        } else {
            return Redirect::route('orders');
        }
    }

    public function orders()
    {
        return view('administrator.orders.table')->withPage('Pedidos');
    }

    public function extraCharge()
    {
        return view('administrator.products.extraCharge')->withPage('Cobro_extra');
    }
}
