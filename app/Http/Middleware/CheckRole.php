<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{

    public function handle($request, Closure $next)
    {

        if (Auth::user()->role == User::ADMIN) {

            return $next($request);
        }

        return redirect()->route('home');
    }
}
