<?php

namespace App\Mail;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyPurchaseAdministrators extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    public function __construct($order_id)
    {

        $order = Order::find($order_id);

        $order->item_qty = json_decode($order->item_qty);
        $order->payment_method = json_decode($order->payment_method);
        $order->user = json_decode($order->user);
        $order->shipping = json_decode($order->shipping);
        $order->direction = json_decode($order->direction);
        $order->billing = json_decode($order->billing);


        $products_items = array();

        foreach ($order->item_qty as $product) {

            $cartItem = Product::where('key', $product->item)->first();

            if ($cartItem) {

                $cartItem['quantity'] =  $product->quantity;

               $products_items[] = $cartItem;
            }
        }

        $order->item_qty = $products_items;


        $this->order = $order;


    }

    public function build()
    {
        return $this->view('emails.NotifyPurchaseAdministrators')
            ->subject('¡Nueva venta realizada!')
            ->with([
                    'title' => '¡Nueva venta realizada!',
                    'order' => $this->order,
                    'url_action' => route('orderDetail', $this->order->id),

                ]

            );
    }
}
