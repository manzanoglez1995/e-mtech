<?php

namespace App\Mail;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class changeStatusOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    public function __construct($order)
    {

        $order->item_qty = json_decode($order->item_qty);
        $order->payment_method = json_decode($order->payment_method);
        $order->user = json_decode($order->user);
        $order->shipping = json_decode($order->shipping);
        $order->direction = json_decode($order->direction);
        $order->billing = json_decode($order->billing);

        $this->order = $order;

    }

    public function build()
    {

        return $this->view('emails.ChangeStatusOrder')
            ->subject('¡Tu compra ya va en camino!')
            ->with([
                    'title' => '¡Tu compra ya va en camino!',
                    'order' => $this->order,
                    'url_action' => route('orderDetailShop', $this->order->id),

                ]

            );
    }
}
