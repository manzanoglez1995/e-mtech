
require('./bootstrap');

window.Vue = require('vue');

//Vue.component('cards', require('./components/inputs/Card.vue').default);

Vue.component('alerts',require('./components/shop/alerts').default);


//Shop
Vue.component('catalogue',require('./components/shop/products/catalogue').default);
Vue.component('ProductDetail',require('./components/shop/products/product-detail.vue').default);
Vue.component('shoppingCart',require('./components/shop/shopping_cart/shopping-cart.vue').default);
Vue.component('cartDetail',require('./components/shop/shopping_cart/cart-detail.vue').default);
Vue.component('checkout',require('./components/shop/shopping_cart/checkout.vue').default);

//Profile
Vue.component('account', require('./components/shop/Profile/account.vue').default);
//Vue.component('cards', require('./components/shop/Profile/cards.vue').default);
Vue.component('purchases', require('./components/shop/Profile/purchases.vue').default);
Vue.component('shippingInformation', require('./components/shop/Profile/shippingInformation.vue').default);
Vue.component('detail', require('./components/shop/Profile/detail.vue').default);

//contact
Vue.component('contact', require('./components/shop/contact/contact.vue').default);




//administrator

//Users
Vue.component('tableUsers', require('./components/administrator/users/table_users.vue').default);
Vue.component('showUsers', require('./components/administrator/users/show_users.vue').default);
//Productos
Vue.component('TableProducts', require('./components/administrator/Products/table.vue').default);
Vue.component('ExtraCharge', require('./components/administrator/Products/extraCharge.vue').default);

//Orders
Vue.component('TableOrders', require('./components/administrator/orders/table_orders.vue').default);
Vue.component('detailOrder', require('./components/administrator/orders/show_orders.vue').default);


//Passport
Vue.component('passport-clients', require('./components/administrator/passport/Clients.vue').default);
Vue.component('passport-authorized-clients', require('./components/administrator/passport/AuthorizedClients.vue').default);
Vue.component('passport-personal-access-tokens',require('./components/administrator/passport/PersonalAccessTokens.vue').default);

window.axios.defaults.baseURL = document.querySelector('meta[name="base-url"]').content;

window.Event = new Vue;

const app = new Vue({
    el: '#app'
});
