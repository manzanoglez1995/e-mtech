@extends('administrator.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <i class="fa fa-edit"></i><label>Usuarios Editar</label>
        </div>
        <div class="card-body">
            <div class="content">

                <show-users user="{{isset($user) ? $user : null }}"></show-users>

            </div>
        </div>
    </div>



@stop
