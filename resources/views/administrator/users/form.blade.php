@extends('administrator.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <i class="fa fa-edit"></i><label>Usuarios</label>
        </div>
        <div class="card-body">
            <div class="content">
                <form-usuarios user="{{isset($user) ? $user : null }}"></form-usuarios>
            </div>
        </div>
    </div>



@stop
