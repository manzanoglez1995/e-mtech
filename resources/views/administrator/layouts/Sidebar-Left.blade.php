<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            {{--
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard') }}">
                            <i class="nav-icon fa fa-tachometer"></i> Dashboard
                        </a>
                    </li>
        --}}
            <li class="nav-item">
                <a class="nav-link" href="{{ route('orders') }}">
                    <i class="nav-icon fa fa-cart-arrow-down"></i>Pedidos</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('extraCharge') }}">
                    <i class="nav-icon fa fa-money"></i>Cobro extra</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('products') }}">
                    <i class="nav-icon fa fa-product-hunt"></i>Productos</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('users') }}">
                    <i class="nav-icon fa fa-user-o"></i>Usuarios</a>
            </li>


            {{--
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="{{ route('users') }}">
                    <i class="nav-icon fa fa-cogs"></i>Configuraciónes</a>
            </li>
            --}}

        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
