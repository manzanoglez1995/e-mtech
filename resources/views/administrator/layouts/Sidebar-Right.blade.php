
<aside class="aside-menu">

    <!-- Tab panes-->

    <div class="tab-content">
        <div class="tab-pane active" id="timeline" role="tabpanel">
            <div class="list-group list-group-accent">
                <div
                    class="list-group-item list-group-item-accent-secondary bg-light text-center
                     font-weight-bold text-muted text-uppercase small">
                    {{ Auth::user()->email }}</div>



                <div style="cursor: pointer" class="list-group-item list-group-item-accent-danger w-100 dropdown-item"
                    <a href=""
                       class="text-decoration-none"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-lock pr-3"></i>
                        <strong>Cerrar Sesión</strong>
                    </a>
                    <form id="logout-form" method="POST" action="{{ route('logout') }}">
                        @csrf
                    </form>

                </div>
            </div>
        </div>
    </div>
</aside>
