@extends('administrator.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <i class="fa fa-edit"></i><label>Productos</label>
        </div>
        <div class="card-body">
            <div class="content">
                <table-products></table-products>
            </div>
        </div>
    </div>


@stop
