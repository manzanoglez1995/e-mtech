@extends('administrator.layout')
@section('content')


    <div class="card">
        <div class="card-header">
            <i class="fa fa-edit"></i><label>Pedidos</label>
        </div>
        <div class="card-body">
            <div class="content">
                <table-orders></table-orders>

            </div>
        </div>
    </div>


@stop
