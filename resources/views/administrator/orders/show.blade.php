@extends('administrator.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <i class="fa fa-edit"></i><label>Pedido</label>
        </div>
        <div class="card-body">
            <div class="content">

                <detail-order order_="{{$order}}"></detail-order>
            </div>
        </div>
    </div>



@stop
