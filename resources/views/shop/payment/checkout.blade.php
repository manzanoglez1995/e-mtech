@extends('shop.layout')
@section('content')

    <div class="breadcrumb-area gray-bg-7">
        <div class="container">



            <checkout
                auth="{{ Auth::check() ? Auth::user() : null }}"
                csrf_token="{{ csrf_token() }}" >

            </checkout>

            @if(isset($message) && isset($type) )
              <alerts
                  message="{{ str_replace(' ','_',$message) }}"
                  type="{{$type}}">
              </alerts>
            @endif

        </div>
    </div>

@endsection
