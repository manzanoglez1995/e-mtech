<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <noscript>
        <strong>Lo sentimos esta pagina requiere de Javascript en tu navegador. Habilítalo para continuar.</strong>
    </noscript>


    <title>{{ config('app.name', 'e-mtech') }}</title>
    <meta charset="utf-8">
    <meta name="base-url" content="{{ route('home') }}"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="robots" content="noindex, follow"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Vue js -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/web/ico-emtech.ico') }}">

    <!-- all css here -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <script src="{{ asset('assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>
    <link href="{{ asset('assets/library/CardJs-master/card-js.min.css') }}" rel="stylesheet" type="text/css"/>

<body>


<div id="app">

    @include("shop.Resources.header")

            @yield('content')

    @include("shop.Resources.footer")

</div>

<!-- vue js -->
<script src="{{ asset('js/app.js') }}"></script>
<!-- all js here -->
<script src="{{ asset('assets/js/vendor/jquery-1.12.0.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/ajax-mail.js') }}"></script>
<script src="{{ asset('assets/js/plugins.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('assets/library/CardJs-master/card-js.min.js') }}"></script>

</body>

</html>









