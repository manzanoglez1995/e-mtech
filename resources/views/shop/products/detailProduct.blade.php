@extends('shop.layout')
@section('content')

    <div class="breadcrumb-area gray-bg-7">
        <div class="container">
            <div class="breadcrumb-content">

                <product-detail keyitem="{{$key}}"></product-detail>

            </div>
        </div>
    </div>

@endsection
