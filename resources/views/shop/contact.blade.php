@extends('shop.layout')
@section('content')


    <div class="contact-us pt-60 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact-page-title mb-40">
                        <h1>
                            Hola, bienvenido
                            <br>
                            Ponte en contacto con nosotros
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <ul class="contact-tab-list nav">
                        <li><a class="active" href="#contact-address" data-toggle="tab">Contacto</a></li>
                        <li><a href="#contact-form-tab" data-toggle="tab">Envíanos un mensaje</a></li>
                       {{-- <li><a href="#store-location" data-toggle="tab">Nuestra ubicación</a></li>--}}
                    </ul>
                </div>
                <div class="col-lg-8">
                    <div class="tab-content tab-content-contact">
                        <div id="contact-address" class="tab-pane fade row d-flex active show">
                            <div class="col-lg-4 col-md-4">
                                <div class="contact-information">
                                    <h4>Dirección</h4>
                                    <p>Guadalajara Jalisco (México)</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="contact-information mrg-top-sm">
                                    <h4>Teléfono</h4>
                                    <p>
                                        <a href="#">01 33 1091 9634</a>
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">

                            </div>
                        </div>
                        <div id="contact-form-tab" class="tab-pane fade row d-flex">
                            <div class="col">


                                <contact></contact>


                            </div>
                        </div>
                        <div id="store-location" class="tab-pane fade row d-flex ">
                            <div class="col-12">
                                <div class="contact-map">
                                    <div id="map"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@stop
