<div class="main-menu main-none shoe-menu">
    <nav>
        <ul>
            <li><a href="{{ route('shop') }}">Tienda </a>

            </li>
            <li><a href="{{route('contact')}}">Contacto</a>

            </li>

            @auth
                <li><a href="#">Mi perfil <i class="ion-chevron-down"></i></a>
                    <ul class="submenu">
                        @if(Auth::user()->role == \App\Models\User::ADMIN)
                            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                        @endif
                        <li><a href="{{ route('account') }}">Cuenta</a></li>
                      {{--<li><a href="{{ route('cards') }}">Mis tarjetas</a></li>--}}
                            <li><a href="{{ route('purchases') }}">Mis compras</a></li>
                        <li><a href="{{ route('shippingInformation') }}">Envios y Facturación</a></li>
                    </ul>
                </li>



                <li><a href="#" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">Salir</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        @csrf
                    </form>
                </li>
            @endauth
            @guest
                <li><a href="{{ route('login') }}">Iniciar Sesión</a></li>
                <li><a href="{{ route('register')  }}"> Registro</a></li>
            @endguest

        </ul>
    </nav>
</div>

