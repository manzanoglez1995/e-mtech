<div class="shoe-mobile-menu border-orange">
    <div class="container-fluid">
        <div class="row">
            <div class="col-8">
                <div class="logo mobile-logo">
                    <a href="{{route('home')}}">
                        {{--  <img src="{{asset('img/logo.png')}}" alt="">--}}
                        <img style="height: 30px" src="{{asset('img/web/logo-emtech.png')}}" alt="">

                    </a>
                </div>
            </div>

            <div class="col-12">

                <div class="mobile-menu-area">
                    <div class="mobile-menu">
                        <nav id="mobile-menu-active">
                            <ul class="menu-overflow">
                                <li><a href="{{ route('shop') }}"> Tienda </a></li>
                                <li><a href="{{route('contact')}}"> Contacto </a></li>

                                @auth
                                    <li><a href="#">Mi perfil</a>
                                        <ul>
                                            @if(Auth::user()->role == \App\Models\User::ADMIN)
                                                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                            @endif
                                            <li><a href="{{ route('account') }}">Cuenta</a></li>
                                           {{--  <li><a href="{{ route('cards') }}">Mis tarjetas</a></li>--}}
                                            <li><a href="{{ route('purchases') }}">Mis compras</a></li>
                                            <li><a href="{{ route('shippingInformation') }}">Envios y Facturación</a>
                                            </li>
                                        </ul>
                                    </li>


                                    <li><a href="#" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                @endauth
                                @guest
                                    <li><a href="{{ route('login') }}">Iniciar Sesión</a></li>
                                    <li><a href="{{ route('register')  }}"> Registro</a></li>
                                @endguest
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
