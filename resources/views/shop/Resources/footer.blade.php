<footer class="footer-area black-bg pt-65">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="footer-widget mb-40">
                    <div class="footer-title mb-30">
                        <h4>Acerca de Nosotros</h4>
                    </div>
                    <div class="footer-about">
                        <p>¡Tenemos la mejor calidad y muchas ofertas!</p>

                    </div>

                    <div class="social-icon mr-40">
                        <ul>
                            <li><a class="facebook" href="https://www.facebook.com/EMtechSolucionesEnDisenoYTecnologia/"><i class="ion-social-facebook"></i></a></li>
                          {{--  <li><a class="twitter" href="#"><i class="ion-social-twitter"></i></a></li>
                            <li><a class="instagram" href="#"><i class="ion-social-instagram-outline"></i></a></li>
                            <li><a class="googleplus" href="https://www.google.com/"><i class="ion-social-googleplus-outline"></i></a> </li>
                            <li><a class="rss" href="#"><i class="ion-social-rss"></i></a></li>
                            <li><a class="dribbble" href="#"><i class="ion-social-dribbble-outline"></i></a></li>
                            <li><a class="vimeo" href="#"><i class="ion-social-vimeo"></i></a></li>
                            <li><a class="pinterest" href="#"><i class="ion-social-pinterest"></i></a></li>
                            <li><a class="skype" href="#"><i class="ion-social-skype-outline"></i></a></li>
                            --}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="footer-widget mb-40">
                    <div class="footer-title mb-30">
                        <h4>Información</h4>
                    </div>
                    <div class="footer-content">
                        <ul>
                            <div class="modal fade" id="NoticeOfPrivacy">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5>Aviso de Privacidad</h5>

                                            <button style="float: left;color: gray;margin-right: 0" type="button"
                                                    class="close"
                                                    data-dismiss="modal">
                                                <span>&times;</span>
                                            </button>

                                        </div>
                                        <div class="modal-body">
                                            <p>Bienvenido a http://www.e-mtech.com y a https://www.e-mtech.com.mx, (los
                                                ‘Sitios’), estos sitios
                                                son propiedad de e-mtech.com, con domicilio isla raza #2492 int 26 col
                                                jardines del sur en
                                                Guadalajara, Jalisco, México. Este Aviso de Privacidad está diseñado
                                                para informarle sobre
                                                nuestras prácticas de recopilación, uso y divulgación de información que
                                                podamos recoger de y
                                                acerca de usted y contempla en todo momento los principios de licitud,
                                                consentimiento,
                                                información, calidad, finalidad, lealtad, proporcionalidad y
                                                responsabilidad en el tratamiento
                                                de los datos personales.
                                                Por favor, asegúrese de leer todo el Aviso de Privacidad antes de usar o
                                                enviar información a
                                                estos sitios. Estos sitios están destinados para su uso por los
                                                residentes de México. Para
                                                cualquier duda relacionada con este aviso favor de comunicarse al
                                                departamento responsable de
                                                datos personales, que es el de jesus.sesma@e-mtech.com, el cual lo puede
                                                contactar en el
                                                teléfono: (33) 10919634 y en el correo: ventas@e-mtech.com en la
                                                dirección isla raza #2492 int
                                                26 col jardines del sur.

                                                Su consentimiento
                                                ________________________________________
                                                Al usar estos sitios, usted está de acuerdo con los términos de este
                                                Aviso de Privacidad.
                                                Siempre que envíe información a través de estos sitios, usted acepta la
                                                recopilación, uso y
                                                divulgación de dicha información de acuerdo con este Aviso de
                                                Privacidad, las Condiciones del
                                                servicio y las disposiciones específicas de estos sitios que pueden ser
                                                presentados en la
                                                información del tiempo se recogen. Los únicos datos que se le
                                                solicitarán será dentro del
                                                carrito de compras y son los siguientes:
                                                No solicitamos datos sensibles en nuestros sitios
                                                • Email
                                                • Password (Personalizada)
                                                • Título
                                                • Nombre (De envío y/o fiscal)
                                                • Apellido (De envío y/o fiscal)
                                                • Teléfono (con lada)
                                                • Calle (De envío y/o fiscal)
                                                • Número (De envío y/o fiscal)
                                                • Código Postal (De envío y/o fiscal)
                                                • Ciudad (De envío y/o fiscal)
                                                • Colonia (De envío y/o fiscal)
                                                • Estado (De envío y/o fiscal)
                                                • País (De envío y/o fiscal)
                                                • Empresa
                                                • RFC
                                                • Nombre de Tarjetahabiente
                                                • Número de tarjeta de crédito/débito
                                                • CVC
                                                • Fecha de vencimiento de tarjeta de crédito/débito
                                                No solicitamos datos sensibles en nuestros sitios.


                                                Uso y divulgación de Información
                                                ________________________________________
                                                E-mtech puede utilizar la información que usted proporciona para mejorar
                                                el contenido de
                                                nuestros sitios, para personalizar la visualización de la web a sus
                                                preferencias, para
                                                comunicarle información (si la ha solicitado), para nuestra
                                                comercialización y fines de
                                                investigación, así mismo utilizamos su información para poder crear un
                                                perfil con contraseña y
                                                pueda realizar sus compras y dar seguimiento a sus pedidos, entregarle
                                                sus productos y
                                                contactarlo en caso de ser necesario y para los fines especificados en
                                                el presente Aviso de
                                                Privacidad.
                                                En nuestro programa de notificación de promociones, ofertas y servicios
                                                a través de correo
                                                electrónico, sólo E-mtech tiene acceso a la información recabada. Este
                                                tipo de publicidad se
                                                realiza mediante avisos y mensajes promocionales de correo electrónico,
                                                los cuales sólo serán
                                                enviados a usted y a aquellos contactos registrados para tal propósito,
                                                esta indicación podrá
                                                usted modificarla en cualquier momento enviándonos un correo a
                                                ventas@e-mtech.com. En los
                                                correos electrónicos enviados, pueden incluirse ocasionalmente ofertas
                                                de terceras partes que
                                                sean nuestros socios comerciales.

                                                Seguridad
                                                ________________________________________
                                                Vamos a tomar medidas razonables para proteger su información personal
                                                cuando usted transmite su
                                                información desde su computadora a nuestros sitios y para proteger esa
                                                información de la
                                                pérdida, mal uso y el acceso no autorizado, revelación, alteración o
                                                destrucción de conformidad
                                                con este Aviso de Privacidad y las Condiciones de usar. Usted debe tener
                                                en cuenta que ninguna
                                                transmisión por Internet es 100% segura o libre de errores, por lo que
                                                una vez recibidos, se
                                                hará todo lo posible por salvaguardar la información. La seguridad y la
                                                confidencialidad de los
                                                datos que los usuarios proporcionen al contratar un servicio o comprar
                                                un producto en línea
                                                estarán protegidos por un servidor seguro bajo el protocolo Secure
                                                Socket Layer (SSL), de tal
                                                forma que los datos enviados se transmitirán encriptados para asegurar
                                                su resguardo. Para
                                                verificar que se encuentra en un entorno protegido asegúrese de que
                                                aparezca una S en la barra
                                                de navegación. Ejemplo: httpS://. En particular, el correo electrónico
                                                enviado desde estos
                                                sitios no puede ser seguro, y por lo tanto debe tener cuidado especial
                                                en decidir qué
                                                información usted nos envía por e-mail. Por otra parte, en la que
                                                utiliza contraseñas, nombres
                                                de usuario, números de identificación u otras características especiales
                                                de acceso en estos
                                                sitios, es su responsabilidad de protegerlos.

                                            </p>

                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="ShippingForms">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5>Formas de envió</h5>

                                            <button style="float: left;color: gray;margin-right: 0" type="button"
                                                    class="close"
                                                    data-dismiss="modal">
                                                <span>&times;</span>
                                            </button>

                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                Resumen
                                                ________________________________________
                                                Creamos el lugar más conveniente para comprar electrónicos en México. Un
                                                aspecto esencial es
                                                asegurar que su pedido llegue a tiempo y en perfectas condiciones con un
                                                costo de logística
                                                justo para usted.
                                                En la práctica, significa que E-mtech se compromete a cumplir con las
                                                siguientes características
                                                de calidad:
                                                • Costo de envío básico desde solo 199 pesos (incluye IVA)
                                                • El tiempo de envío estándar es de 1 a 4 días hábiles
                                                • Desglosamos el costo de envío real en su carrito de compras
                                                • Usted ahorra en el envío en la compra de varios productos
                                                • Aseguramos todos los envíos contra daños, pérdida o robo – sin
                                                deducibles, sin límites, sin
                                                costo al cliente (si el cliente así lo desea)
                                                • Manejamos productos sensibles y sabemos empacarlos para máxima
                                                protección durante el envío
                                                • En todo momento el estatus de su envío se puede consultar en línea a
                                                través de un número de
                                                guía.


                                                Formas y tiempos de entrega
                                                ________________________________________
                                                Envió estándar
                                                El tiempo de entrega por mensajería varía dependiendo del destino y
                                                cuenta con un plazo de
                                                entrega de 1 a 4 días hábiles. Todos los envíos se realizan una vez que
                                                se haya recibido el
                                                importe total de la venta.


                                                NO CONTAMOS CON PISO DE EXHIBICIÓN. POR RAZONES DE SEGURIDAD, NO SE
                                                PERMITEN RECOLECCIONES DE
                                                MERCANCÍA. GRACIAS POR SU COMPRENSIÓN.


                                                Costo de envío
                                                ________________________________________
                                                E-mtech mantiene transparencia acerca del costo real de su envío
                                                • El costo de envío es igual en todo México
                                                • El costo ya incluye IVA y se suma al precio del producto al momento de
                                                comprar
                                                • El costo de envío puede ser visto en el carro de ventas
                                                En la compra de más de un producto o cada producto
                                                • El costo de envío se basa en el precio de un paquete grande que es más
                                                económico que varios
                                                paquetes pequeños. ¡Usted ahorra en los costos de envío!
                                                • El costo del envío será calculado en el último paso del proceso de
                                                pedido


                                                Seguro del envío
                                                ________________________________________
                                                Aseguramos todos los envíos con nuestros socios de logística para
                                                brindarle máxima seguridad en
                                                sus compras en E-mtech
                                                • El seguro se contrata por E-mtech y no tiene ningún costo adicional
                                                para el cliente
                                                • El monto cubre 100% del valor de la mercancía – sin deducibles, sin
                                                límites
                                                • El seguro cubre daños, pérdida o robo
                                                • Todos nuestros envíos están asegurados
                                                Por supuesto prevenimos daños en el envío. Manejamos productos sensibles
                                                y sabemos empacarlos
                                                para máxima protección.


                                                AVISO IMPORTANTE
                                                Para mantener la validez del seguro, le recordamos siempre verificar los
                                                siguientes puntos al
                                                recibir su mercancía:
                                                • Inspección visual para corroborar que el empaque viene en óptimas
                                                condiciones
                                                • Verificar que las cintas y sellos no hayan sido violados
                                                • Al firmar de recibido, verificar que firma únicamente por las guías
                                                que le han sido entregadas
                                                » AVISO IMPORTANTE: Para hacer válido el seguro de envío, es
                                                indispensable revisar los paquetes
                                                al momento de la entrega, es importante reportarlo dentro de las
                                                primeras 24 horas. ¡Revise el
                                                contenido y no acepte paquetes dañados!


                                                Rastreo en línea
                                                ________________________________________
                                                • Cada paquete puede ser rastreado en línea usando su número de guía.
                                                • Si realizó la compra de más de un producto, es posible que los
                                                productos sean enviados en
                                                diferentes paquetes con varios números de guía.
                                            </p>

                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="PaymentMethods">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5>Métodos De Pago</h5>

                                            <button style="float: left;color: gray;margin-right: 0" type="button"
                                                    class="close"
                                                    data-dismiss="modal">
                                                <span>&times;</span>
                                            </button>

                                        </div>
                                        <div class="modal-body">
                                            Resumen
                                            ________________________________________
                                            Nuestra selección de formas de pago incluye:
                                            • Pago referenciado (Transferencia SPEI, Depósito en efectivo/cheque)
                                            • Tarjeta de Débito/Crédito (VISA, MasterCard, AMEX)
                                            • Paypal
                                            Todos los pedidos se facturan. Puede consultar sus facturas en el área de
                                            Mis pedidos »


                                            Pago referenciado (Transferencia SPEI, Depósito en efectivo/cheque)
                                            ________________________________________

                                            • Recibimos transferencias electrónicas SPEI desde cualquier banco y
                                            depósitos en efectivo/cheque en
                                            sucursales Banamex.
                                            • Pagos por transferencia electrónica SPEI se reconocen automáticamente
                                            dentro de 60 minutos,
                                            depósitos en efectivo/cheque dentro de 24 horas.
                                            • Transferencias electrónicas SPEI por montos de hasta 8 mil pesos ahora se
                                            pueden realizar las 24
                                            horas del día, los 7 días de la semana.
                                            • Cada cliente recibe datos bancarios personalizados para efectuar su pago
                                            vía correo electrónico al
                                            momento de colocar su pedido.
                                            • Es necesario recibir su pago en firme antes de finalizar el siguiente día
                                            hábil para evitar que su
                                            pedido sea cancelado.
                                            • No es necesario enviar comprobantes de pago ya que cada pago se identifica
                                            automáticamente.


                                            Tarjeta de Débito/Crédito (VISA, MasterCard, AMEX)
                                            ________________________________________

                                            • Asegure el procesamiento más rápido pagando con tarjetas de débito/crédito
                                            VISA, MasterCard y
                                            American Express nacionales e internacionales.
                                            • El cobro a la tarjeta se realiza después de confirmar su pedido en el
                                            último paso del proceso. Por
                                            mientras, no se realiza ningún cargo.
                                            • Solo necesita los datos impresos en su tarjeta: Nombre del
                                            tarjetahabiente, número de tarjeta,
                                            fecha de vencimiento y código de seguridad.
                                            • Tarjetas de débito aceptadas: BBVA Bancomer, Banorte, Banamex, Banco
                                            Azteca, Banco del Bajío,
                                            Bancoppel, BanRegio, HSBC, Inbursa y Santander.


                                            Paypal
                                            ________________________________________

                                            • Utilice su cuenta PayPal para realizar pagos con tarjetas de
                                            débito/crédito VISA, MasterCard y
                                            American Express nacionales e internacionales.
                                            • El cobro a la tarjeta se realiza después de confirmar su pedido en el
                                            último paso del proceso. Por
                                            mientras, no se realiza ningún cargo.
                                            • Solo necesita los datos impresos en su tarjeta: Nombre del
                                            tarjetahabiente, número de tarjeta,
                                            fecha de vencimiento y código de seguridad.
                                            • Tarjetas de débito aceptadas: BBVA Bancomer, Banorte, Banamex, Banco
                                            Azteca, Banco del Bajío,
                                            Bancoppel, BanRegio, HSBC, Inbursa y Santander.


                                            Facturación
                                            ________________________________________
                                            Facturamos cada pedido en estricto apego a los lineamientos de las
                                            autoridades fiscales. Si requiere
                                            una factura con sus datos fiscales vigentes, por favor ingrese los datos de
                                            facturación incluyendo
                                            su RFC durante el proceso de pedido. Si no requiere una factura, solo
                                            complete los datos básicos y
                                            le expedimos una factura genérica.
                                            Le enviamos su factura vía correo electrónico al momento de embarcar su
                                            pedido. Posteriormente,
                                            puede consultar el historial de sus facturas en el área de Mis pedidos


                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="warranty">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5>Garantía</h5>

                                            <button style="float: left;color: gray;margin-right: 0" type="button"
                                                    class="close"
                                                    data-dismiss="modal">
                                                <span>&times;</span>
                                            </button>

                                        </div>
                                        <div class="modal-body">
                                            Garantía del Fabricante
                                            ________________________________________
                                            Todos nuestros productos tienen una garantía directamente del fabricante,
                                            ésta entra en vigor a
                                            partir de la fecha de compra que aparece en el comprobante que recibe al
                                            momento de pagar su pedido.
                                            En caso de que el producto adquirido presente algún desperfecto o falla, el
                                            cliente deberá dirigirse
                                            directamente con el fabricante para hacer válida su garantía. En la parte
                                            inferior podrá encontrar
                                            los datos de contacto de cada empresa., ahí le podrán informar sobre alguna
                                            solución remota,
                                            dirección de envío de producto para validez de garantía o sobre algún centro
                                            de servicio cercano a
                                            su localidad.

                                            Debido a que E-mtech no cuenta con centro de servicio no podrá aceptar los
                                            casos de garantía que se
                                            presenten. En dado caso que el fabricante no cuente con centro de servicio
                                            en México ó no se le esté
                                            dando la atención merecida, puede enviarnos un correo a contacto@e-mtech.com
                                            con su nombre completo,
                                            fecha de compra, modelo y número de serie. Posteriormente se le responderá
                                            con pasos a seguir.


                                            IMPORTANTE: Le suplicamos que revise el paquete y su contenido al momento de
                                            la llegada. Por favor
                                            no acepte la entrega en caso de notar algún daño ó piezas faltantes, ya que
                                            con su firma confirma la
                                            llegada en orden a la paquetería y no podrá ser válido el seguro de la
                                            misma. Todos los gastos
                                            incurridos en la aplicación de la garantía deberán ser cubiertos por parte
                                            del cliente.


                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="ReturnPolicy">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5>Política de devolución</h5>

                                            <button style="float: left;color: gray;margin-right: 0" type="button"
                                                    class="close"
                                                    data-dismiss="modal">
                                                <span>&times;</span>
                                            </button>

                                        </div>
                                        <div class="modal-body">
                                            Política de devoluciones e-mtech.com
                                            El plazo de devoluciones en nuestra tienda online es de 24 horas desde la
                                            recepción del pedido.

                                            En ningún caso el cliente debe devolver la mercancía e-mtech.com por sus
                                            propios medios. E-mtech no
                                            se hará responsable de la mercancía si el cliente la devuelve por sus
                                            propios medios.


                                            Para devolver cualquier artículo de nuestra tienda online contacte en
                                            contacto@e-mtech.com.


                                            A continuación, le informamos cómo proceder en cada caso:

                                            1. Producto dañado durante el envío

                                            Al recibir el producto debe revisarlo bien. Si comprueba que ha sido dañado
                                            durante el envío deberá
                                            ponerse en contacto con nosotros de forma inmediata a través de nuestro
                                            correo:
                                            contacto@e-mtech.com, donde le indicaremos la forma de proceder.


                                            2. Producto incorrecto

                                            En el caso de que nos hayamos equivocado en el envío del producto, dispone
                                            de 24 horas naturales a
                                            partir de la fecha de recepción del pedido para devolvernos el mismo. Por
                                            favor, contacte con
                                            nuestro servicio de atención al cliente en la dirección de correo
                                            electrónico contacto@e-mtech.com.


                                            3. Producto defectuoso

                                            Para la devolución por producto defectuoso, primero habrá que constatar el
                                            defecto de la pieza
                                            mediante el envío de fotos en las que se compruebe que realmente el elemento
                                            está dañado o
                                            deteriorado. A partir de aquí todo corre de nuestro cargo al correo de
                                            contacto@e-mtech.com.


                                            4. ¿Cuáles son los gastos de devolución?

                                            Si nos devuelve un producto debido a un error por parte de E-mtech, es decir
                                            error en el envío,
                                            nosotros siempre asumiremos todos los gastos de la devolución.

                                            En ningún caso el cliente debe devolver la mercancía a E-mtech por sus
                                            propios medios. E-mtech no se
                                            hará responsable de la mercancía si el cliente la devuelve por sus propios
                                            medios.

                                            No serán susceptibles de cambio ni devolución los productos realizados con
                                            medidas especiales o
                                            fabricados de encargo conforme a las especificaciones del cliente,
                                            exceptuando aquellos que estén
                                            defectuosos o incorrectamente suministrados.

                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <li><a data-toggle="modal" data-target="#NoticeOfPrivacy" href="#">Aviso de privacidad</a>
                            </li>
                            <li><a data-toggle="modal" data-target="#ShippingForms" href="#">Formas de envió </a></li>
                            <li><a data-toggle="modal" data-target="#PaymentMethods" href="#">Formas de pago</a></li>
                            <li><a data-toggle="modal" data-target="#warranty" href="#">Garantía</a></li>
                            <li><a data-toggle="modal" data-target="#ReturnPolicy" href="#">Política de devoluciones</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            @auth
                <div class="col-lg-2 col-md-6">
                    <div class="footer-widget mb-40">
                        <div class="footer-title mb-30">
                            <h4>Mi Cuenta</h4>
                        </div>
                        <div class="footer-content">
                            <ul>


                                @if(Auth::user()->role == \App\Models\User::ADMIN)
                                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                @endif
                                <li><a href="{{ route('account') }}">Cuenta</a></li>
                               {{-- <li><a href="{{ route('cards') }}">Mis tarjetas</a></li>--}}
                                    <li><a href="{{ route('purchases') }}">Mis compras</a></li>
                                <li><a href="{{ route('shippingInformation') }}">Datos de envió</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            @endauth
            <div class="col-lg-4 col-md-6">
                <div class="footer-widget mb-40">
                    <div class="footer-title mb-30">
                        <h4>Contacto</h4>
                    </div>
                    <div class="footer-contact">
                        <ul>
                            <li>Teléfono: 01 33 1091 9634</li>
                            <li>Correo electrónico: info@e-mtech.com</li>
                        </ul>
                    </div>
                    <div class="opening-time">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="border-top-3 mt-25 pt-65 pb-65">
            <div class="row">
                <div class="col-12">
                    <div class="footer-middle text-center">
                        <div class="footer-tag">

                            <!-- <ul> ABAJO
                                 <li><a href="#">Online Shopping </a></li>
                                 <li><a href="#">Promotions </a></li>

                                 <li><a href="#">Help </a></li>
                                 <li><a href="#">Customer Service </a></li>
                                 <li><a href="#">Support </a></li>
                                 <li><a href="#">Most Populars </a></li>
                                 <li><a href="#">New Arrivals </a></li>
                                 <li><a href="#">Special Products </a></li>
                                 <li><a href="#">Manufacturers </a></li>
                                 <li><a href="#">Our Stores </a></li>
                                 <li><a href="#">Shipping </a></li>
                                 <li><a href="#">Payments </a></li>
                                 <li><a href="#">Warantee </a></li>
                                 <li><a href="#">Refunds </a></li>
                                 <li><a href="#">Checkout </a></li>
                                 <li><a href="#">Discount </a></li>
                                 <li><a href="#">Terms & Conditions </a></li>
                                 <li><a href="#">Policy Shipping </a></li>
                                 <li><a href="#">Returns </a></li>
                                 <li><a href="#">Refunds</a></li>
                             </ul>
                         -->

                        </div>
                        <div class="payment-icon mt-20">
                            <a href="#">
                                <img src="assets/img/icon-img/payment.png" alt="">
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom black-bg-2 pb-30 pt-25">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="copyright text-center">
                        <p>Copyright © <a href="#">e-mtech</a>. All Right Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
