
<header class="header-area header-padding shoe-header header-fixed">

    <div class="header-middle pt-40 pb-40 pl-40 pr-40 border-orange header-middle-color-2 header-middle-color-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-9 col-lg-12 col-md-12 col-12">
                    <div class="logo logo-mrg">
                        <a href="{{route('home')}}">
                          {{--  <img src="{{asset('img/logo.png')}}" alt="">--}}
                            <img style="height: 30px" src="{{asset('img/web/logo-emtech.png')}}" alt="">
                        </a>
                    </div>

                    @include("shop.Resources.Menu.menuWeb")
                </div>

                <div class="col-xl-3 col-lg-12 col-md-12 col-12">


                    <shopping-cart></shopping-cart>


                </div>
            </div>
        </div>
    </div>




                    @include("shop.Resources.Menu.menuMobile")



</header>

<div style="height: 130px"></div>



@if (
     \Route::current()->getName() != 'login' and
     \Route::current()->getName() != 'register' and
     \Route::current()->getName() != 'password.request' and
     \Route::current()->getName() != 'password.reset')

    {{ \Breadcrumbs::render($page, isset($parameter) ? $parameter: null) }}

@endif
