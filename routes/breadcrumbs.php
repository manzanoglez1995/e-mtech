<?php
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;


// Shop breadcrumbs

// Home
Breadcrumbs::for('contact', function ($breadcrumbs) {
    $breadcrumbs->push('Contacto', route('contact'));
});
Breadcrumbs::for('home', function ($breadcrumbs) {
    $breadcrumbs->push('Inicio', route('shop'));
});
Breadcrumbs::for('shop', function ($breadcrumbs) {
    $breadcrumbs->push('Tienda', route('shop'));
});
// Tienda >[key]
Breadcrumbs::for('detail-product', function ($breadcrumbs,$key) {
    $breadcrumbs->parent('shop');
    $breadcrumbs->push("Producto", route('product',$key));
});
Breadcrumbs::for('cart', function ($breadcrumbs) {
    $breadcrumbs->push('Carrito', route('cart'));
});
Breadcrumbs::for('checkout', function ($breadcrumbs) {
    $breadcrumbs->parent('cart');
    $breadcrumbs->push('pago', route('checkout'));
});
Breadcrumbs::for('detatilProduct', function ($breadcrumbs) {
    $breadcrumbs->push('Producto', route('product'));
});
//Shop profile
Breadcrumbs::for('account', function ($breadcrumbs) {
    $breadcrumbs->parent('shop');
    $breadcrumbs->push('Cuenta', route('account'));
});
Breadcrumbs::for('cards', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('Mis tarjetas', route('cards'));
});
Breadcrumbs::for('purchases', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('Mis Compras', route('purchases'));
});

Breadcrumbs::for('orderDetailShop', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('purchases');
    $breadcrumbs->push('Detalle', route('orderDetailShop',$id));
});

Breadcrumbs::for('orderDetail', function ($breadcrumbs) {
    $breadcrumbs->parent('shoppingHistory');
    $breadcrumbs->push('Detalle', route('orderDetail'));
});
Breadcrumbs::for('shippingInformation', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('Datos de envío', route('shippingInformation'));
});





// Backend




//Administrador backend
Breadcrumbs::for('Dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('dashboard'));
});
// dashboard > usuarios
Breadcrumbs::for('Usuarios', function ($breadcrumbs) {
    $breadcrumbs->parent('Dashboard');
    $breadcrumbs->push('Usuario', route('users'));
});
// dashboard > usuarios > mostrar[id]
Breadcrumbs::for('MostrarUsuarios', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('Usuarios');
    $breadcrumbs->push('Detalle', route('users_edit',$id));
});
// dashboard > productos
Breadcrumbs::for('Productos', function ($breadcrumbs) {
    $breadcrumbs->parent('Dashboard');
    $breadcrumbs->push('Productos', route('products'));

});// dashboard > pedidos
Breadcrumbs::for('Pedidos', function ($breadcrumbs) {
    $breadcrumbs->parent('Dashboard');
    $breadcrumbs->push('Pedidos', route('orders'));
});

// dashboard > pedidos > detalle
Breadcrumbs::for('Detalle', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('Pedidos');
    $breadcrumbs->push('Detalle', route('orderDetail',$id));
});

// dashboard > cobro extra
Breadcrumbs::for('Cobro_extra', function ($breadcrumbs) {
    $breadcrumbs->parent('Dashboard');
    $breadcrumbs->push('Cobro extra', route('extraCharge'));
});



