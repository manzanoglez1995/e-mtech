<?php


// rutas de autenticacion
Auth::routes();

// Rutas tienda

Route::get('/', 'Shop\ShopController@shop')->name('home');
Route::get('/tienda', 'Shop\ShopController@shop')->name('shop');
Route::get('/tienda/{key}', 'Shop\ShopController@detailProduct')->name('product');
Route::get('/contacto', 'Shop\ShopController@contact')->name('contact');
Route::get('/carrito', 'Shop\ShopController@cart')->name('cart');
Route::get('/pago', 'Shop\ShopController@checkout')->name('checkout');


// rutas de tienda autenticado
Route::group(['middleware' => ['auth']], function () {

    Route::get('/cuenta', 'Shop\ShopController@account')->name('account');
   // Route::get('/tarjetas', 'Shop\ShopController@cards')->name('cards');
    Route::get('/mis_compras', 'Shop\ShopController@purchases')->name('purchases');
    Route::get('/datos_envio', 'Shop\ShopController@shippingInformation')->name('shippingInformation');
    Route::get('/detalle_pedido/{id}', 'Shop\ShopController@orderDetail')->name('orderDetailShop');

});


// rutas de administrador
Route::group(['middleware' => ['auth', 'role']], function () {

    Route::group(['prefix' => 'dashboard'], function () {

      //  Route::get('/', 'Administrator\AdministratorController@dashboard')->name('dashboard');
        Route::get('/', 'Administrator\AdministratorController@redirect')->name('dashboard');
        Route::get('/usuarios', 'Administrator\AdministratorController@users')->name('users');
        Route::get('/usuarios/{id}', 'Administrator\AdministratorController@editUsers')->name('users_edit');
        Route::get('/productos', 'Administrator\AdministratorController@products')->name('products');
        Route::get('/pedidos', 'Administrator\AdministratorController@orders')->name('orders');
        Route::get('/pedidos/{id}' ,'Administrator\AdministratorController@detailOrders')->name('orderDetail');
        Route::get('/cobro_extra', 'Administrator\AdministratorController@extraCharge')->name('extraCharge');

    });
});

//Paypal callback
Route::group(['prefix' => 'payment'], function () {
    Route::post('/','Shop\PaymentController@payment')->name('payment');
    Route::get('/status','Shop\PaymentController@getPaymentStatus')->name('paymentStatus');
});





