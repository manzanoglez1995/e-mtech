<?php

use Illuminate\Http\Request;


Route::group(['prefix' => 'user'], function () {

    $controller = 'Api\UserController@';

    Route::get('/', $controller . 'users');
    Route::post('/login', $controller . 'login');
    Route::post('/register', $controller . 'register');
    Route::post('/update/{id}', $controller . 'update');
    Route::post('/search', $controller . 'search');
    Route::delete('/delete/{user_id}', $controller . 'delete');

    Route::post('/changeRole', $controller . 'changeRole');

    //only with auth By token
    Route::get('/AuthInfo', $controller . 'AuthInfo');
    Route::post('/logout', $controller . 'logout');


});

Route::group(['prefix' => 'product'], function () {

    $controller = 'Api\ProductController@';

    Route::get('/', $controller . 'show');
    Route::get('/all', $controller . 'products');
    Route::get('/synchronize', $controller . 'synchronize');
    Route::post('/search', $controller . 'search');
    Route::post('/shoppingCart', $controller . 'shoppingCart');
    Route::post('/quoteCart', $controller . 'quoteCart');
    Route::post('/findByKey', $controller . 'findByKey');
    Route::get('/brands', $controller . 'list_brands');
    Route::post('/mostSelledProducts', $controller . 'mostSelledProducts');


    Route::post('/simpleSearch', $controller . 'simpleSearch');
    Route::post('/getSearchKeyword', $controller . 'getSearchKeyword');
    Route::post('/addSearchKeyword', $controller . 'addSearchKeyword');
    Route::post('/destroySearchKeyword', $controller . 'destroySearchKeyword');

});

Route::group(['prefix' => 'UserBilling'], function () {

    $controller = 'Api\UserBillingController@';

    Route::get('/{user_id}', $controller . 'show');
    Route::post('/create', $controller . 'create');
    Route::post('/{billing_id}/update', $controller . 'update');
    Route::delete('/{billing_id}', $controller . 'destroy');

});

Route::group(['prefix' => 'UserAdress'], function () {

    $controller = 'Api\UserShippingAddressesController@';

    Route::get('/{user_id}', $controller . 'show');
    Route::post('/create', $controller . 'create');
    Route::post('/{adress_id}/update', $controller . 'update');
    Route::delete('/{adress_id}', $controller . 'destroy');

});

Route::group(['prefix' => 'CreditCard'], function () {

    $controller = 'Api\UserCreditCardController@';

    Route::get('/{user_id}', $controller . 'show');
    Route::post('/create', $controller . 'create');
    Route::post('/{credit_card_id}/update', $controller . 'update');
    Route::delete('/{credit_card_id}', $controller . 'destroy');

});

Route::group(['prefix' => 'orders'], function () {

    $controller = 'Api\OrderController@';
    Route::post('/create', $controller . 'create');
    Route::post('/search', $controller . 'search');
    Route::get('/', $controller . 'show');
    Route::get('/{order_id}', $controller . 'find');
    Route::post('getPaymentStatus', $controller . 'getPaymentStatus');

});

Route::group(['prefix' => 'ExtraCharge'], function () {

    $controller = 'Api\ExtraChargeController@';

    Route::get('/', $controller . 'show');
    Route::post('/create', $controller . 'create');
    Route::post('/{extraCharge_id}/update', $controller . 'update');
    Route::delete('/{extraCharge_id}', $controller . 'destroy');
    Route::post('/search', $controller . 'search');
    Route::get('/groups', $controller . 'showActives');

});

Route::group(['prefix' => 'estates'], function () {
    $controller = 'Api\EstatesController@';
    Route::get('/', $controller . 'show');
});

Route::group(['prefix' => 'contact'], function () {

    $controller = 'Api\UserController@';

    Route::post('/message', $controller . 'contactMessage');
});


Route::group(['prefix' => 'order'], function () {
    $controller = 'Api\OrderController@';
    Route::post('/checkStatus', $controller . 'checkStatus');
    Route::post('/getOrdersByUser', $controller . 'getOrdersByUser');
    Route::post('/sendEmailTest', $controller . 'sendEmailTest');
    Route::post('/find', $controller . 'find');
    Route::post('/setShipping', $controller . 'setShipping');
    Route::post('/finishOrder', $controller . 'finish_order');

});



