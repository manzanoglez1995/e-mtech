<?php

use Illuminate\Database\Seeder;

class GroupsProducts extends Seeder
{


    public function run()
    {

        $url = "http://www.grupocva.com/catalogo_clientes_xml/grupos.xml";

        $content = utf8_encode(file_get_contents($url));
        $groups = simplexml_load_string($content);

        foreach($groups as $group){

            $extracharge = new \App\Models\ExtraCharge();

            $extracharge->group = $group[0];
            $extracharge->commission_price = 0;
            $extracharge->shipping_price = 0;
            $extracharge->status = true;

            $extracharge->save();
        }

    }
}
