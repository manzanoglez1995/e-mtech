<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserShippingAddressesTable extends Migration
{

    public function up()
    {
        Schema::create('user_shipping_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('street');
            $table->string('external_number');
            $table->string('internal_number')->nullable();
            $table->string('reference')->nullable();
            $table->string('colony');
            $table->string('cp');
            $table->string('city');
            $table->integer('estate_id')->default(15);
            $table->string('country')->default('México');
            $table->integer('user_id')->unsigned();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::table('user_shipping_addresses', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });


    }


    public function down()
    {
        Schema::dropIfExists('user_shipping_addresses');
    }
}
