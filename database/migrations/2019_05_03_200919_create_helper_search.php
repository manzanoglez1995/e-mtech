<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelperSearch extends Migration
{

    public function up()
    {
        Schema::create('helper_searches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group');
            $table->string('keyword')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('helper_searches');
    }
}
