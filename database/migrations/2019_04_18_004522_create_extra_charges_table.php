<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraChargesTable extends Migration
{

    public function up()
    {
        Schema::create('extra_charges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group');
            $table->double('commission_price');
            $table->double('shipping_price');
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('extra_charges');
    }
}
