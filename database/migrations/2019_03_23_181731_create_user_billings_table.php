<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBillingsTable extends Migration
{

    public function up()
    {
        Schema::create('user_billings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nameOrSocialReason');
            $table->string('RFC');
            $table->integer('user_id')->unsigned();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::table('user_billings', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

    }


    public function down()
    {
        Schema::dropIfExists('user_billings');
    }
}
