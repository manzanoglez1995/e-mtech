<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProduct extends Migration
{

    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->unique();
            $table->string('manufacturer_code');
            $table->text('description');
            $table->string('principal');
            $table->string('group');
            $table->string('brand');
            $table->string('warranty');
            $table->string('class');
            $table->integer('available');
            $table->double('price');
            $table->string('currency');
            $table->text('data_sheet')->nullable();
            $table->text('bussiness_card')->nullable();
            $table->string('image')->nullable();
            $table->string('AvailableCD')->nullable();
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('users');
    }
}
