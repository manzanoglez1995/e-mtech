<?php
return array(
    // set your paypal credential

    //sandbox
    //'client_id' => 'AY9eVGgyUo2ygTnKKLSo7ORBq_m7dRZIbAcEgM9nLGs69v4tVyoXit6Jg5w9eUx9CYQW-8lpa0mkvKFR',
   // 'secret' => 'EEq6m6nryBrkvyjfKhK02aiN1hf6L4GxdDUQtLVUjPg9txoCFtioHJwz5G86a8HFF9BdIxG16fQRIPI6',

    //production
   'client_id' => 'AYbugfZI1ouMEYwoxu9KQ8T2_H22jT7GwdBBxCxFur0IjdbSUvAW-_J_RBhjHpHVG2eJbYlhhCRb7Xzg',
    'secret' => 'EBtNONHjuuhYgy3_tikEIe2v7mjta3aHeKoS4OhRO-O8Vhx7vnw4Csmrw5rRUEb7enCHdBQGBZRO_wXg',
    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
       // 'mode' => 'sandbox',
       'mode' => 'live',
        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,
        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,
        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);
